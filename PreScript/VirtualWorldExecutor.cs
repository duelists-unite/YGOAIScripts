using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using DuelBot;
using DuelBot.Game;
using Enumerator;
using DuelBot.Game.AI;

namespace DuelBot.Game.AI.Decks
{
    /** compatibility change
    [Deck("Virtual World", "AI_VirtualWorld")]
    */
    class VirtualWorldExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Virtual World", DuelRules.MasterDuel, (ai, duel) => new VirtualWorldExecutor(ai, duel),
                "M2cwsnnDdF1qPiMMf8v5xQTDxfG2jBaL1FlheBLnWyYYNg9oZQDhp5f3w7GcSwATDNvzSjH1f2dhhWE91ccMMLwugZMFhO8r/2eCYSWPmQww3Pj6NBgfrBBjXCXoyFRVeI5Bbq8kw/pdzaz9e34y72jkZDb4+pPxVHMn0/Tnkkz/GO4xb4xkYp2umMK6hLuadUayB+OSFG+wmQA=");
        }

        bool DEBUG = false; // whether is in debug

        Dictionary<int, bool> oncePerTurn = new Dictionary<int, bool>();
        Dictionary<int, bool> normalSummoned = new Dictionary<int, bool>();
        
        List<int> spellTargets = new List<int>()
        {
            CardId.VWGate_Qinglong,
            CardId.VWCity_Kauwloon
        };

        List<int> trapTargets = new List<int>()
        {
            CardId.VWGate_Chuche,
            CardId.VWGate_Xuanwu
        };

        List<int> monsterTargets = new List<int>()
        {
            CardId.VW_Lulu,
            CardId.VW_Jiji,
            CardId.VW_Lili,
            CardId.VW_Laolao,
            CardId.VW_NiangNiang,
            // CardId.VW_Shenshen
        };

        List<int> handTraps = new List<int>()
        {
            CardId.Nibiru,
            CardId.PSYFramegearGamma,
            CardId.InfiniteImpermanence
        };

        Dictionary<string, bool> knownComboPaths = new Dictionary<string, bool>()
        {
            {"Lulu+City", false},
            {"Lulu+Qing", false},
            {"Lili+City", false},
            {"Lili+Qing", false},
            {"Jiji+City", false},
            {"Lao+City", false},
            {"Lulu+Lulu", false},
            {"Lulu+Niang", false},
            {"Lulu+Lili", false},
            {"Lulu+Jiji", false},
            {"Lulu+Lao", false},
            {"Jiji+Jiji/Niang", false},
            {"Jiji+Lao", false},
            {"Jiji+Lili", false},
            {"Lili+Jiji/Niang", false},
            {"Niang+Lao", false},
            {"Lulu+Jiji+Lili+L3", false},
            {"Lulu+Jiji+Lili", false},
            {"Lulu+Jiji+Niang", false},
            {"Lao+Jiji+Niang", false},
            {"other", false},
            {"brick", false}
        };

        int settable = 0;

        public class CardId
        {
            public const int InfiniteImpermanence = 10045474; // x3
            public const int Nibiru = 27204311;               // x3
            public const int PSYFrameDriver = 49036338;
            public const int PSYFramegearGamma = 38814750;    // x3
            public const int VW_NiangNiang = 8736823;         // x3
            public const int VW_Lili = 49966326;              // x3
            public const int VW_Laolao = 86483512;            // x2
            public const int VW_Lulu = 49088914;              // x3
            public const int VW_Jiji = 12571621;              // x3
            public const int VW_Toutou = 20799347;
            public const int VWCity_Kauwloon = 14886190;      // x3
            public const int VWGate_Qinglong = 50275295;      // x3
            public const int VWGate_Chuche = 13364097;        // x2
            public const int VWGate_Xuanwu = 18249921;
            public const int EmergencyTeleport = 67723438;    // x2
            public const int PotOfDesires = 35261759;
            public const int PotOfProsperity = 84211599;     // x3
            // extra
            public const int VermillionDragonMech = 66698383;
            public const int VW_Shenshen = 92519087;
            public const int FA_DawnDragster = 33158448;
            public const int StardustChargeWarrior = 64880894;
            public const int CoralDragon = 42566602;
            public const int DivineArsenalZEUS = 90448279;
            public const int Number39UtopiaBeyond = 21521304;
            public const int VW_JiuJiu = 35252119;
            public const int ConstellarPtolemyM7 = 38495396;
            public const int GaiaDragon = 91949988;
            public const int UltimayaTzolkin = 1686814;
            public const int MuddyMudragon = 84040113;
            public const int InvokedCaliga = 13529466;
            public const int DarkDragoon = 37818794;
            public const int CrystalWingSynchroDragon = 50954680;
        }

        public VirtualWorldExecutor(GameAI ai, BotDuel duel) : base(ai, duel)
        {
            // in this script executors are defined and deleted dynamically
        }

        public override bool OnSelectHand()
        {
            return true;
        }

        public override void OnChainEnd()
        {
            // draw effects
            if (
                (oncePerTurn.ContainsKey(CardId.PotOfProsperity) && oncePerTurn[CardId.PotOfProsperity]) ||
                (oncePerTurn.ContainsKey(CardId.PotOfDesires) && oncePerTurn[CardId.PotOfDesires])
                // oncePerTurn.ContainsKey(CardId.StardustChargeWarrior) ||
                // oncePerTurn.ContainsKey(CardId.CoralDragon)
            ) {
                BootComboPaths();
                if (DEBUG) {
                    Console.WriteLine("REBOOTING COMBO PATHS");
                    List<string> paths = knownComboPaths.Where(x => x.Value == true)
                        .Select(x => x.Key)
                        .ToList();

                    foreach(string path in paths) {
                        Console.WriteLine(path);
                    }
                }
                oncePerTurn[CardId.PotOfProsperity] = false;
                oncePerTurn[CardId.PotOfDesires] = false;
            }
        }

        public override void OnNewTurn()
        {
            ResetExecutors();
            oncePerTurn = new Dictionary<int, bool>();
            normalSummoned = new Dictionary<int, bool>();
            knownComboPaths = knownComboPaths.ToDictionary(p => p.Key, p => false);

            BootComboPaths();
            settable = TzolkinWinCon();

            if (DEBUG) {
                List<string> paths = knownComboPaths
                    .Where(path => path.Value == true)
                    .Select(path => path.Key)
                    .ToList();

                foreach(string path in paths) {
                    Console.WriteLine(path);
                }
            }

            // backrow effects
            AddExecutor(ExecutorType.Activate, CardId.VWGate_Chuche, VWGate_ChuchePopEff);
            AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_XuanwuReposEff);
            AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_QinglongNegateEff);
            // hand traps
            AddExecutor(ExecutorType.Activate, CardId.PSYFramegearGamma, PSYFramegearGamma);
            AddExecutor(ExecutorType.Activate, CardId.Nibiru, Nibiru);
            AddExecutor(ExecutorType.Activate, CardId.InfiniteImpermanence, DefaultInfiniteImpermanence); // TODO: use salad version
            // pot effects
            AddExecutor(ExecutorType.Activate, CardId.PotOfProsperity, PotOfProsperity);
            AddExecutor(ExecutorType.Activate, CardId.PotOfDesires, PotOfDesires);
            AddExecutor(ExecutorType.Activate, CardId.EmergencyTeleport, EmergencyTeleport);

            AddExecutor(ExecutorType.Activate, CardId.DarkDragoon, DarkDragoon);
            AddExecutor(ExecutorType.Activate, CardId.CrystalWingSynchroDragon, CrystalWingSynchroDragon);
            AddExecutor(ExecutorType.SpSummon, CardId.VermillionDragonMech, VermillionDragonMech);
            AddExecutor(ExecutorType.Activate, CardId.VermillionDragonMech, VermillionDragonMech);
            AddExecutor(ExecutorType.Activate, CardId.VW_Shenshen, VW_Shenshen);

            AddExecutor(ExecutorType.Repos, MonsterRepos);

            if (knownComboPaths["Lulu+City"]) {
                AddExecutor(ExecutorType.Activate, CardId.VWCity_Kauwloon, VWCity_Kauwloon);
                // select chuche
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                // target chuche, send qing, add lili

                // settable = TzolkinWinCon();
                // if (settable != 0) {} // other less greedy path if already settable in hand
                // if (settable && (Bot.HasInHand(CardId.PSYFrameDriver) || false)) {} // other less greedy path with Level 3 in hand
                // opsional saffer path here without set fodder
                // opsional path with set fodder to counter vw/trib/backrow

                AddExecutor(ExecutorType.Activate, CardId.VW_Lili, VW_Lili);
                // target chuche, send jiji, send qing
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // banish from gy, add lao, dump not lao
                AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                // target chuche, send niang, sp jiji
                AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                // trigger gy sp eff
                AddExecutor(ExecutorType.SpSummon, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // sp charge with jiji and niang
                AddExecutor(ExecutorType.Activate, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // draw eff, update set fodder
                if (settable != 0) {
                    // opsional safer path here with set fodder

                    AddExecutor(ExecutorType.SpSummon, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // summon M7 with lili and charge
                    AddExecutor(ExecutorType.Activate, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // eff select gy, discard lili, add jiji
                    AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                    // target lao, send xuan, ... (add lulu)
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                    // xuan gy eff, target lili, dump least value
                    AddExecutor(ExecutorType.SpSummon, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    // select lao, select lili, sp in def
                    AddExecutor(ExecutorType.SpellSet, settable);
                    // set fodder from hand
                    AddExecutor(ExecutorType.Activate, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    // eff select crystan wing
                    AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                    // sp select lulu, select jiji
                    AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                    // eff select caliga, select muddy, select tzolkin, sp in def
                } else {
                    AddExecutor(ExecutorType.SpSummon, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // summon M7 with lili and charge
                    AddExecutor(ExecutorType.Activate, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // eff select gy, discard lili, add jiji
                    AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                    // target lao, send xuan, ... (add lulu)
                    AddExecutor(ExecutorType.SpSummon, CardId.GaiaDragon, GaiaDragon);
                    // select m7
                    AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                    // select lulu and jiji
                    AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                    // select dragoon, select muddy, select gaia
                    AddExecutor(ExecutorType.Activate, CardId.DarkDragoon, DarkDragoon);
                    // by default: negate at first opportunity
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                    // xuan gy eff, target jiji, dump least value
                    AddExecutor(ExecutorType.SpSummon, CardId.VW_Shenshen, VW_Shenshen);
                    // sp shen, select lao, select jiji
                 }
            } else if (knownComboPaths["Lulu+Qing"]) {
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // activate if not on field
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                // select qing, send xuan, add jiji
                AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                // select lulu, send qing, add lulu
                AddExecutor(ExecutorType.SpSummon, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // select lulu, select jiji
                AddExecutor(ExecutorType.Activate, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // eff draw
                if (settable != 0) {
                    // opsional path with set fodder to counter vw/trib/backrow

                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                    // gy ef, add lili, dump least value
                    AddExecutor(ExecutorType.Activate, CardId.VW_Lili, VW_Lili);
                    // select qing, send lao, send chuche
                    AddExecutor(ExecutorType.SpSummon, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // select charge, select lili
                    AddExecutor(ExecutorType.Activate, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // select gy, discard lili, add lao
                    AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                    // select qing, send niang, sp jiji
                    AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                    // trigger gy eff
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                    // gy eff, revive lili, dump least value
                    AddExecutor(ExecutorType.SpSummon, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    // select lao, select lili
                    AddExecutor(ExecutorType.SpellSet, settable);
                    // set fodder from hand
                    AddExecutor(ExecutorType.Activate, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    // eff select crystan wing
                    AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                    // sp select niang, select jiji
                    AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                    // banish eff: select xuan
                    AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                    // eff select caliga, select muddy, select tzolkin, sp in def
                } else {
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                    // gy ef, add lili, dump least value
                    AddExecutor(ExecutorType.Activate, CardId.VW_Lili, VW_Lili);
                    // select qing, send lao, send chuche
                    AddExecutor(ExecutorType.SpSummon, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // select charge, select lili
                    AddExecutor(ExecutorType.Activate, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // select gy, discard lili, add lao
                    AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                    // select qing, send niang, sp jiji
                    AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                    // trigger gy eff
                    AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                    // select niang, select jiji
                    AddExecutor(ExecutorType.SpSummon, CardId.GaiaDragon, GaiaDragon);
                    // select m7
                    AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                    // select dragoon, select muddy, select gaia dragon
                    AddExecutor(ExecutorType.Activate, CardId.DarkDragoon, DarkDragoon);
                    // default: negate first thing
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                    // gy eff, revive jiji, dump least value
                    AddExecutor(ExecutorType.SpSummon, CardId.VW_Shenshen, VW_Shenshen);
                    // summon in def
                }
            } else if (knownComboPaths["Lili+City"]) {
                AddExecutor(ExecutorType.Activate, CardId.VWCity_Kauwloon, VWCity_Kauwloon);
                // select chuche
                AddExecutor(ExecutorType.Activate, CardId.VW_Lili, VW_Lili);
                // select chuche, send qing, send niang
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // gy eff, add lulu, dump least value
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                // select chuche, send qing, add lao
                if (settable != 0) {
                    AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                    // select lulu, send xuan, don't revive (?)
                    AddExecutor(ExecutorType.SpSummon, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    // select lao, select lili
                    AddExecutor(ExecutorType.SpellSet, settable);
                    AddExecutor(ExecutorType.Activate, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    // select crystal wing
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                    // gy eff, select niang, dump least value
                    AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                    // select lulu, select niang
                    AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                    // select caliga, select muddy, select tzolkin
                } else {
                    AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                    // gy eff
                    AddExecutor(ExecutorType.SpSummon, CardId.VW_Shenshen, VW_Shenshen);
                    // select lulu, select lili
                    AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                    // select chuche, send jiji, revive lili
                    AddExecutor(ExecutorType.SpSummon, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // select lao, select lili
                    AddExecutor(ExecutorType.Activate, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // from gy, send lili, add jiji
                    AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                    // select niang, send xuan, add lulu
                    AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                    // select niang, select jiji
                    AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                    // banish eff, select qing
                    AddExecutor(ExecutorType.SpSummon, CardId.GaiaDragon, GaiaDragon);
                    // select m7
                    AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                    // select dragoon, select muddy, select gaia
                    AddExecutor(ExecutorType.Activate, CardId.DarkDragoon, DarkDragoon);
                }
            } else if (knownComboPaths["Lili+Qing"]) {
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // acc if not on field
                AddExecutor(ExecutorType.Activate, CardId.VW_Lili, VW_Lili);
                // select qing, dump xuan, dump lulu
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                // gy eff revive lulu, dump least value
                AddExecutor(ExecutorType.SpSummon, CardId.VermillionDragonMech, VermillionDragonMech);
                // select lili, select lulu
                AddExecutor(ExecutorType.Activate, CardId.VermillionDragonMech, VermillionDragonMech);
                // select lulu, select vermillion
                AddExecutor(ExecutorType.Activate, CardId.VermillionDragonMech, VermillionDragonMech);
                // gy eff, add lulu

                if (settable != 0) {
                    AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                    // select qing, send chuche, add jiji
                    AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                    // select lulu, send qing, add lulu
                    AddExecutor(ExecutorType.SpSummon, CardId.StardustChargeWarrior, StardustChargeWarrior);
                    // select lulu, select jiji
                    AddExecutor(ExecutorType.Activate, CardId.StardustChargeWarrior, StardustChargeWarrior);
                    // draw eff
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                    // gy eff, add lao, dump least value
                    AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                    // select qing, dump niang, revive jiji
                    AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                    // gy eff
                    AddExecutor(ExecutorType.SpSummon, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    AddExecutor(ExecutorType.Activate, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    // select lao, select charge
                    AddExecutor(ExecutorType.SpellSet, settable);
                    AddExecutor(ExecutorType.Activate, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    // select crystal wing
                    AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                    // select niang, select jiji
                    AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                    // banish eff, select xuan
                    AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                    // select dragoon, select muddy, select tzolkin
                    AddExecutor(ExecutorType.Activate, CardId.DarkDragoon, DarkDragoon);
                } else {
                    AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                    // select qing, send chuche, add lao
                    AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                    // select lulu, send qing, revive lili
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                    // gy eff, add jiji, dump least value
                    AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                    // select qing, send niang, add lulu
                    AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                    // select lulu, select jiji
                    AddExecutor(ExecutorType.SpSummon, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // select lili, select lao
                    AddExecutor(ExecutorType.Activate, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // select lili, add jiji
                    AddExecutor(ExecutorType.SpSummon, CardId.GaiaDragon, GaiaDragon);
                    // select m7
                    AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                    // select dragoon, select muddy, select gaia
                    AddExecutor(ExecutorType.Activate, CardId.DarkDragoon, DarkDragoon);
                    AddExecutor(ExecutorType.Summon, CardId.VW_Jiji, VW_Jiji);
                    AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                    // gy eff
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Chuche, VWGate_Chuche);
                    // gy eff select jiji, select +3
                    AddExecutor(ExecutorType.SpSummon, CardId.VW_Shenshen, VW_Shenshen);
                    // select niang, select jiji
                    AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                    // banish eff, select chuche
                }
            } else if (knownComboPaths["Jiji+City"]) {
                AddExecutor(ExecutorType.Activate, CardId.VWCity_Kauwloon, VWCity_Kauwloon);
                // select chuche
                AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                // select chuche, send qing, add (settable ? lulu : lao)
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // gy eff add lulu, dump least value
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                // select chuche, send qing, add lao
                AddExecutor(ExecutorType.SpSummon, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // select lulu, select jiji
                AddExecutor(ExecutorType.Activate, CardId.StardustChargeWarrior, StardustChargeWarrior);
                AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                // select chuche, send niang, revive jiji
                AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                // gy eff
                if (settable != 0) {
                    AddExecutor(ExecutorType.SpSummon, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    // select lao, select charge
                    AddExecutor(ExecutorType.SpellSet, settable);
                    AddExecutor(ExecutorType.Activate, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    // select crystal wing
                    AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                    // select niang, select jiji
                    AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                    // select caliga, select muddy, select tzolkin
                } else {
                    AddExecutor(ExecutorType.SpSummon, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // select charge, select lao
                    AddExecutor(ExecutorType.Activate, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // select lao, add lulu
                    AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                    // select niang, select jiji
                    AddExecutor(ExecutorType.SpSummon, CardId.GaiaDragon, GaiaDragon);
                    // select m7
                    AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                    // select dragoon, select muddy, select gaia
                    AddExecutor(ExecutorType.Activate, CardId.DarkDragoon, DarkDragoon);
                }
            } else if (knownComboPaths["Lao+City"]) {
                AddExecutor(ExecutorType.Activate, CardId.VWCity_Kauwloon, VWCity_Kauwloon);
                // select chuche
                AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                // select chuche, send qing
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // gy eff, add lulu, dump least value
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                // select chuche, send qing, add jiji
                AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                // select chuche, send niang, add lulu
                AddExecutor(ExecutorType.SpSummon, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // select lulu, select jiji
                AddExecutor(ExecutorType.Activate, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // draw eff
                AddExecutor(ExecutorType.SpSummon, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                // select lao, select charge
                AddExecutor(ExecutorType.Activate, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                // select lao, add jiji
                AddExecutor(ExecutorType.Summon, CardId.VW_Jiji, VW_Jiji);
                AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                // gy eff
                AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                // select niang, select jiji
                AddExecutor(ExecutorType.SpSummon, CardId.GaiaDragon, GaiaDragon);
                // select m7
                AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                // select dragoon, select muddy, select gaia
                AddExecutor(ExecutorType.Activate, CardId.DarkDragoon, DarkDragoon);
            } else if (knownComboPaths["Lulu+Lulu"]) {
                AddExecutor(ExecutorType.Summon, CardId.VW_Lulu, VW_Lulu_Summon);
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                // select lulu, send qing, add chuche
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // gy eff, add jiji, dump
                AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                // select lulu, send chuche, add lulu
                AddExecutor(ExecutorType.SpSummon, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // select lulu, select jiji
                AddExecutor(ExecutorType.Activate, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // draw eff
                AddExecutor(ExecutorType.SpSummon, CardId.VW_Shenshen, VW_Shenshen);
                // select lulu, select charge
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Chuche, VWGate_Chuche);
                // select shen
                AddExecutor(ExecutorType.SpellSet, CardId.VWGate_Chuche);
            } else if (knownComboPaths["Lulu+Niang"]) {
                AddExecutor(ExecutorType.Summon, CardId.VW_NiangNiang, VW_NiangNiang_Summon);
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                // select niang, send qing, add chuche
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // gy eff, add lao, dump
                AddExecutor(ExecutorType.SpSummon, CardId.VW_JiuJiu, VW_JiuJiu);
                AddExecutor(ExecutorType.Activate, CardId.VW_JiuJiu, VW_JiuJiu);
                // select lulu, select niang
                AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                // select jiu, send xuan, revive niang
                AddExecutor(ExecutorType.SpSummon, CardId.UltimayaTzolkin, UltimayaTzolkin);
                // select lao, select jiu
                AddExecutor(ExecutorType.SpellSet, CardId.VWGate_Chuche);
                AddExecutor(ExecutorType.Activate, CardId.UltimayaTzolkin, UltimayaTzolkin);
                // select crystal wing
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                // gy eff, revive lulu, dump
                AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                // select lulu, select niang
                AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                // select caliga, select muddy, select tzolkin
            } else if (knownComboPaths["Lulu+Lili"]) {
                AddExecutor(ExecutorType.Summon, CardId.VW_Lulu, VW_Lulu_Summon);
                AddExecutor(ExecutorType.Activate, CardId.VW_Lili, VW_Lili);
                // select lulu, send qing, send xuan
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // gy eff, add lulu, dump
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                // select lili, send chuche, add city
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Chuche, VWGate_Chuche);
                // gy eff, select lulu, +3
                AddExecutor(ExecutorType.SpSummon, CardId.UltimayaTzolkin, UltimayaTzolkin);
                // select lulu, select lili
                AddExecutor(ExecutorType.SpellSet, CardId.VWCity_Kauwloon);
                AddExecutor(ExecutorType.Activate, CardId.UltimayaTzolkin, UltimayaTzolkin);
                // select crystal wing
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                // gy eff, select lili, dump
                AddExecutor(ExecutorType.SpSummon, CardId.VW_Shenshen, VW_Shenshen);
                // select lulu, select lili
                AddExecutor(ExecutorType.Activate, CardId.VWCity_Kauwloon, VWCity_Kauwloon);
                // select chuche
            } else if (knownComboPaths["Lulu+Jiji"]) {
                AddExecutor(ExecutorType.Summon, CardId.VW_Jiji, VW_Jiji);
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                // select jiji, send qing, add chuche
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // gy eff, add lao, dump
                AddExecutor(ExecutorType.SpSummon, CardId.VW_JiuJiu, VW_JiuJiu);
                // select lulu, select jiji
                AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                // select jiu, send xuan, select lulu
                AddExecutor(ExecutorType.SpSummon, CardId.UltimayaTzolkin, UltimayaTzolkin);
                // select lao, select jiu
                AddExecutor(ExecutorType.SpellSet, CardId.VWGate_Chuche);
                AddExecutor(ExecutorType.Activate, CardId.UltimayaTzolkin, UltimayaTzolkin);
                // select crystal wing
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                // gy eff, select jiji, dump
                AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                // select lulu, select jiji
                AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                // select caliga, select muddy, select tzolkin
            } else if (knownComboPaths["Lulu+Lao"]) {
                AddExecutor(ExecutorType.Summon, CardId.VW_Lulu, VW_Lulu_Summon);
                AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                // select lulu, send qing
                if (settable != 0) {
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                    // gy eff, add lili, dump
                    AddExecutor(ExecutorType.Activate, CardId.VW_Lili, VW_Lili);
                    // select lao, send xuan, send qing
                    AddExecutor(ExecutorType.SpSummon, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    // select lao, select lili
                    AddExecutor(ExecutorType.SpellSet, settable);
                    AddExecutor(ExecutorType.Activate, CardId.UltimayaTzolkin, UltimayaTzolkin);
                    // select crystal wing
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                    // gy eff, select lili, dump
                    AddExecutor(ExecutorType.SpSummon, CardId.VW_Shenshen, VW_Shenshen);
                    // select lulu, select lili
                } else {
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                    // gy eff, add jiji, dump
                    AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                    // select lao, send xuan, add lulu
                    AddExecutor(ExecutorType.SpSummon, CardId.StardustChargeWarrior, StardustChargeWarrior);
                    // select lulu, select jiji
                    AddExecutor(ExecutorType.Activate, CardId.StardustChargeWarrior, StardustChargeWarrior);
                    // draw eff
                    AddExecutor(ExecutorType.SpSummon, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // select charge, select lao
                    AddExecutor(ExecutorType.Activate, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                    // select lao, add lulu
                    AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                    // gy eff, select jiji, dump
                    AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                    // select jiji, send qing, add chuche
                    AddExecutor(ExecutorType.SpSummon, CardId.GaiaDragon, GaiaDragon);
                    // select m7
                    AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                    // select lulu, select jiji
                    AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                    // select dragoon, select muddy, select gaia
                    AddExecutor(ExecutorType.Activate, CardId.DarkDragoon, DarkDragoon);
                    AddExecutor(ExecutorType.SpellSet, CardId.VWGate_Chuche);
                }
            } else if (knownComboPaths["Jiji+Jiji/Niang"]) {
                AddExecutor(ExecutorType.Summon, CardId.VW_NiangNiang, VW_NiangNiang);
                AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                // select niang, send qing, add lulu
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // gy eff, add lulu, dump
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                // select niang, send xuan, add city
                AddExecutor(ExecutorType.SpSummon, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // select lulu, select niang
                AddExecutor(ExecutorType.Activate, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // draw eff
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                // gy eff, select lulu, dump
                AddExecutor(ExecutorType.SpSummon, CardId.CoralDragon, CoralDragon);
                // select lulu, select jiji
                AddExecutor(ExecutorType.SpSummon, CardId.UltimayaTzolkin, UltimayaTzolkin);
                // select coral, select change
                AddExecutor(ExecutorType.Activate, CardId.CoralDragon, CoralDragon);
                // gy eff, draw
                AddExecutor(ExecutorType.SpellSet, CardId.VWCity_Kauwloon, VWCity_Kauwloon);
                AddExecutor(ExecutorType.Activate, CardId.UltimayaTzolkin, UltimayaTzolkin);
                // select crystal wing
            } else if (knownComboPaths["Jiji+Lao"]) {
                AddExecutor(ExecutorType.Summon, CardId.VW_Jiji, VW_Jiji);
                AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                // select jiji, send qing
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // gy eff, select lulu, dump
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                // select jiji, send xuan, add city
                AddExecutor(ExecutorType.SpSummon, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // select lulu, select jiji
                AddExecutor(ExecutorType.Activate, CardId.StardustChargeWarrior, StardustChargeWarrior);
                // draw eff
                AddExecutor(ExecutorType.SpSummon, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                // select charge, select lao
                AddExecutor(ExecutorType.Activate, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                // select lao, add jiji
                AddExecutor(ExecutorType.Activate, CardId.VWCity_Kauwloon, VWCity_Kauwloon);
                // select chuche
                AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                // select chuche, send qing, add lulu
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                // gy eff, select lulu, dump
                AddExecutor(ExecutorType.SpSummon, CardId.GaiaDragon, GaiaDragon);
                // select m7
                AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                // select lulu, select jiji
                AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                // select dragoon, select muddy, select gaia
                AddExecutor(ExecutorType.Activate, CardId.DarkDragoon, DarkDragoon);
            // } else if (knownComboPaths["Jiji+Lili"]) {
                //
            // } else if (knownComboPaths["Lili+Jiji/Niang"]) {
                //
            } else if (knownComboPaths["Niang+Lao"]) {
                AddExecutor(ExecutorType.Summon, CardId.VW_NiangNiang, VW_NiangNiang_Summon);
                AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                // select niang, send qing
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                // gy eff select lulu, dump
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                // select niang, send chuche, add city
                AddExecutor(ExecutorType.SpSummon, CardId.VW_Shenshen, VW_Shenshen);
                // select lao, select niang
                AddExecutor(ExecutorType.Activate, CardId.VWCity_Kauwloon, VWCity_Kauwloon);
                // select chuche
            // } else if (knownComboPaths["Lulu+Jiji+Lili"]) {
                //
            // } else if (knownComboPaths["Lulu+Jiji+Niang"]) {
                //
            // } else if (knownComboPaths["Lao+Jiji+Niang"]) {
                //
            } else {
                
                // other starter spells
                AddExecutor(ExecutorType.Activate, CardId.EmergencyTeleport, EmergencyTeleport);
                AddExecutor(ExecutorType.Activate, CardId.VWCity_Kauwloon, VWCity_Kauwloon);
                // boss monsters
                AddExecutor(ExecutorType.Activate, CardId.DarkDragoon, DarkDragoon);
                AddExecutor(ExecutorType.Activate, CardId.CrystalWingSynchroDragon, CrystalWingSynchroDragon);
                AddExecutor(ExecutorType.SpSummon, CardId.VermillionDragonMech, VermillionDragonMech);
                AddExecutor(ExecutorType.Activate, CardId.VermillionDragonMech, VermillionDragonMech);
                AddExecutor(ExecutorType.SpSummon, CardId.VW_Shenshen, VW_Shenshen);
                AddExecutor(ExecutorType.Activate, CardId.VW_Shenshen, VW_Shenshen);
                AddExecutor(ExecutorType.SpSummon, CardId.Number39UtopiaBeyond, Number39UtopiaBeyond);
                AddExecutor(ExecutorType.Activate, CardId.Number39UtopiaBeyond, Number39UtopiaBeyond);
                AddExecutor(ExecutorType.SpSummon, CardId.DivineArsenalZEUS, DivineArsenalZEUS);
                AddExecutor(ExecutorType.Activate, CardId.DivineArsenalZEUS, DivineArsenalZEUS);
                // extra deck helpers
                AddExecutor(ExecutorType.SpSummon, CardId.StardustChargeWarrior, StardustChargeWarrior);
                AddExecutor(ExecutorType.Activate, CardId.StardustChargeWarrior, StardustChargeWarrior);
                AddExecutor(ExecutorType.SpSummon, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                AddExecutor(ExecutorType.Activate, CardId.ConstellarPtolemyM7, ConstellarPtolemyM7);
                AddExecutor(ExecutorType.SpSummon, CardId.GaiaDragon, GaiaDragon);
                AddExecutor(ExecutorType.Activate, CardId.GaiaDragon, GaiaDragon);
                AddExecutor(ExecutorType.SpSummon, CardId.MuddyMudragon, MuddyMudragon);
                AddExecutor(ExecutorType.Activate, CardId.MuddyMudragon, MuddyMudragon);
                AddExecutor(ExecutorType.SpSummon, CardId.UltimayaTzolkin, UltimayaTzolkin);
                if (settable != 0) {
                    AddExecutor(ExecutorType.SpellSet, settable);
                }
                AddExecutor(ExecutorType.Activate, CardId.UltimayaTzolkin, UltimayaTzolkin);
                AddExecutor(ExecutorType.SpSummon, CardId.VW_JiuJiu, VW_JiuJiu);
                AddExecutor(ExecutorType.Activate, CardId.VW_JiuJiu, VW_JiuJiu);
                AddExecutor(ExecutorType.SpSummon, CardId.CoralDragon, CoralDragon);
                AddExecutor(ExecutorType.Activate, CardId.CoralDragon, CoralDragon);
                // main deck helpers
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Xuanwu, VWGate_Xuanwu);
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Chuche, VWGate_Chuche);
                AddExecutor(ExecutorType.Activate, CardId.VWGate_Qinglong, VWGate_Qinglong);
                AddExecutor(ExecutorType.Activate, CardId.VW_Lulu, VW_Lulu);
                AddExecutor(ExecutorType.Activate, CardId.VW_Jiji, VW_Jiji);
                AddExecutor(ExecutorType.Activate, CardId.VW_Lili, VW_Lili);
                AddExecutor(ExecutorType.Activate, CardId.VW_Laolao, VW_Laolao);
                AddExecutor(ExecutorType.Summon, CardId.VW_Jiji, VW_Jiji);                
                AddExecutor(ExecutorType.Summon, CardId.VW_Lulu, VW_Lulu_Summon);
                AddExecutor(ExecutorType.Activate, CardId.VW_NiangNiang, VW_NiangNiang);
                AddExecutor(ExecutorType.Summon, CardId.VW_NiangNiang, VW_NiangNiang_Summon);
                AddExecutor(ExecutorType.Activate, CardId.VW_Toutou, VW_Toutou);
                AddExecutor(ExecutorType.Summon, CardId.VW_Toutou, VW_Toutou_Summon);
            }

            if (!Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                List<int> chucheTargets = monsterTargets.Concat(spellTargets).Concat(trapTargets).ToList();

                if (Bot.Banished.Where(card => chucheTargets.Any(y => y == card.Id)).Count() > 1) {
                    AddExecutor(ExecutorType.Activate, CardId.VWCity_Kauwloon, VWCity_Kauwloon);
                    AddExecutor(ExecutorType.SpellSet, CardId.VWGate_Chuche);
                }
            }

            AddExecutor(ExecutorType.SpellSet, CardId.VWGate_Chuche, SpellSet);
            AddExecutor(ExecutorType.SpellSet, CardId.InfiniteImpermanence, SpellSet);

        }

        private bool PSYFramegearGamma()
        {
            AI.SelectPosition(CardPosition.FaceUpDefence);
            AI.SelectPosition(CardPosition.FaceUpDefence);
            return BotDuel.LastChainPlayer == 1;
        }

        private bool Nibiru()
        {
            if (
                Bot.HasInMonstersZone(new List<int>() {
                    CardId.DarkDragoon,
                    CardId.CrystalWingSynchroDragon,
                    CardId.InvokedCaliga
                }, true)
            ) {
                return false;
            } else if (BotDuel.Player != 0) {
                int totalAtk = 0;
                int totalDef = 0;
                foreach(BotClientCard card in Enemy.GetMonsters()) {
                    totalAtk += card.Attack;
                    totalDef += card.Defense;
                }

                if (totalAtk < 3000 || Util.IsTurn1OrMain2()) {
                    AI.SelectPosition(CardPosition.FaceUpAttack);
                    AI.SelectPosition(CardPosition.FaceUpDefence);
                } else {
                    AI.SelectPosition(CardPosition.FaceUpDefence);
                    AI.SelectPosition(CardPosition.FaceUpDefence);
                }
                
                return true;
            }
            return false;
        }

        private bool VW_Toutou_Summon()
        {
            if (!Bot.HasInMonstersZone(CardId.VW_Toutou) && !normalSummoned.ContainsKey(CardId.VW_Toutou)) {
                if (knownComboPaths["other"] && !Bot.HasInHand(CardId.VW_NiangNiang)) {
                    normalSummoned[CardId.VW_Toutou] = true;
                    return true;
                }
            }
            return false;
        }

        private bool VW_Toutou()
        {
            if (
                knownComboPaths["other"] &&
                Bot.HasInHand(new List<int>() {
                    CardId.PSYFramegearGamma,
                    CardId.PSYFrameDriver
                }) &&
                Bot.GetMonsters().Count(card => card.Level == 3 || card.Level == 6) > 0
            ) {
                AI.SelectCard((Bot.HasInHand(CardId.PSYFrameDriver) ? CardId.PSYFrameDriver : CardId.PSYFramegearGamma));
                oncePerTurn[CardId.VW_Toutou] = true;
                return true;
            }
            return false;
        }

        private bool VW_NiangNiang_Summon()
        {
            if (!Bot.HasInMonstersZone(CardId.VW_NiangNiang) && !normalSummoned.ContainsKey(CardId.VW_NiangNiang)) {
                if (knownComboPaths["Lulu+Niang"] || knownComboPaths["Niang+Lao"]) {
                    normalSummoned[CardId.VW_NiangNiang] = true;
                    return true;
                } else if (knownComboPaths["other"]) {
                    normalSummoned[CardId.VW_NiangNiang] = true;
                    return true;
                }
            }
            return false;
        }
        private bool VW_NiangNiang()
        {
            if (DynamicCard.Location == CardLocation.Grave) {
                if (knownComboPaths["Lili+City"] && settable == 0) {
                    return false;
                }
                return true;
            }
            if (DynamicCard.Location == CardLocation.Removed) {
                if (knownComboPaths["Lulu+Qing"]) {
                    AI.SelectCard(CardId.VWGate_Xuanwu);
                    return true;
                } else if (knownComboPaths["Lili+Qing"]) {
                    AI.SelectCard(settable != 0 ? CardId.VWGate_Xuanwu : CardId.VWGate_Chuche);
                    return true;
                } else if (knownComboPaths["other"]) {
                    if (Bot.HasInBanished(CardId.VWGate_Chuche)) {
                        AI.SelectCard(CardId.VWGate_Chuche);
                    }
                    return !Bot.HasInSpellZone(CardId.VWGate_Chuche);
                }
            }
            return false;
        }

        private bool VW_Lulu_Summon()
        {
            if (!Bot.HasInMonstersZone(CardId.VW_Lulu) && !normalSummoned.ContainsKey(CardId.VW_Lulu)) {
                if (
                    knownComboPaths["Lulu+Lulu"] ||
                    knownComboPaths["Lulu+Lili"] ||
                    knownComboPaths["Lulu+Lao"]
                ) {
                    normalSummoned[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["other"]) {
                    if (!Bot.HasInHand(new List<int> {
                        CardId.VW_NiangNiang,
                        CardId.VW_Toutou
                    })) {
                        normalSummoned[CardId.VW_Lulu] = true;
                        return true;
                    }
                }
            }
            return false;
        }
        
        private bool VW_Lulu()
        {
            if (!oncePerTurn.ContainsKey(CardId.VW_Lulu)) {
                if (knownComboPaths["Lulu+City"] && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VW_Lili);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Qing"] && Bot.HasInSpellZone(CardId.VWGate_Qinglong)) {
                    AI.SelectCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VWGate_Xuanwu);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["Lili+City"] && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VW_Laolao);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["Lili+Qing"] && Bot.HasInSpellZone(CardId.VWGate_Qinglong)) {
                    AI.SelectCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(settable != 0 ? CardId.VW_Jiji : CardId.VW_Laolao);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["Jiji+City"] && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VW_Laolao);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["Lao+City"] && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Lulu"] && Bot.HasInMonstersZone(CardId.VW_Lulu)) {
                    AI.SelectCard(CardId.VW_Lulu);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VWGate_Chuche);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Niang"] && Bot.HasInMonstersZone(CardId.VW_NiangNiang)) {
                    AI.SelectCard(CardId.VW_NiangNiang);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VWGate_Chuche);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Lili"] && Bot.HasInMonstersZone(CardId.VW_Lili)) {
                    AI.SelectCard(CardId.VW_Lili);
                    AI.SelectNextCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VWCity_Kauwloon);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Jiji"] && Bot.HasInMonstersZone(CardId.VW_Jiji)) {
                    AI.SelectCard(CardId.VW_Jiji);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VWGate_Chuche);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Lao"] && settable == 0 && Bot.HasInMonstersZone(CardId.VW_Jiji)) {
                    AI.SelectCard(CardId.VW_Jiji);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    AI.SelectYesNo(true);
                    AI.SelectNextCard(CardId.VWGate_Chuche);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["Jiji+Lao"] && Bot.HasInMonstersZone(CardId.VW_Jiji)) {
                    AI.SelectCard(CardId.VW_Jiji);
                    AI.SelectNextCard(CardId.VWGate_Xuanwu);
                    AI.SelectNextCard(CardId.VWCity_Kauwloon);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["Niang+Lao"] && Bot.HasInMonstersZone(CardId.VW_NiangNiang)) {
                    AI.SelectCard(CardId.VW_NiangNiang);
                    AI.SelectNextCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VWCity_Kauwloon);
                    oncePerTurn[CardId.VW_Lulu] = true;
                    return true;
                } else if (knownComboPaths["other"]) {
                    if (Bot.HasInMonstersZone(CardId.InvokedCaliga) && !Bot.HasInHandOrInSpellZone(CardId.VWGate_Qinglong)) {
                        AI.SelectCard(CardId.VWGate_Chuche);
                        AI.SelectNextCard(CardId.VW_NiangNiang);
                        AI.SelectNextCard(CardId.VWGate_Qinglong);
                        oncePerTurn[CardId.VW_Lulu] = true;
                        return true;
                    } else {
                        if (Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                            AI.SelectCard(CardId.VWGate_Chuche);
                            AI.SelectNextCard(CardId.VWGate_Qinglong);
                        } else if (Bot.HasInSpellZone(CardId.VWGate_Qinglong)) {
                            AI.SelectCard(CardId.VWGate_Qinglong);
                            AI.SelectNextCard(CardId.VWGate_Chuche);
                        } else {
                            AI.SelectCard(CardId.VW_Shenshen);
                            AI.SelectNextCard(CardId.VWGate_Qinglong);
                        }

                        if (!Bot.HasInHand(CardId.VW_Jiji)) {
                            AI.SelectNextCard(CardId.VW_Jiji);
                        } else {
                            AI.SelectNextCard(CardId.VW_Laolao);
                        }
                        oncePerTurn[CardId.VW_Lulu] = true;
                        return true;
                    }
                }
            }

            return false;
        }

        private bool VW_Laolao()
        {
            if (!oncePerTurn.ContainsKey(CardId.VW_Laolao)) {
                if (knownComboPaths["Lulu+City"] && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VW_NiangNiang);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    oncePerTurn[CardId.VW_Laolao] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Qing"] && Bot.HasInSpellZone(CardId.VWGate_Qinglong)) {
                    AI.SelectCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VW_NiangNiang);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    oncePerTurn[CardId.VW_Laolao] = true;
                    return true;
                } else if (knownComboPaths["Lili+City"]) {
                    if (settable != 0 && Bot.HasInMonstersZone(CardId.VW_Lulu)) {
                        AI.SelectCard(CardId.VW_Lulu);
                        AI.SelectNextCard(CardId.VWGate_Xuanwu);
                        AI.SelectYesNo(false);
                    } else if(settable == 0 && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                        AI.SelectCard(CardId.VWGate_Chuche);
                        AI.SelectNextCard(CardId.VW_Jiji);
                        AI.SelectNextCard(CardId.VW_Lili);
                    } else {
                        return false;
                    }
                    oncePerTurn[CardId.VW_Laolao] = true;
                    return true;
                } else if (knownComboPaths["Lili+Qing"]) {
                    if (settable != 0 && Bot.HasInSpellZone(CardId.VWGate_Qinglong)) {
                        AI.SelectCard(CardId.VWGate_Qinglong);
                        AI.SelectNextCard(CardId.VW_NiangNiang);
                        AI.SelectNextCard(CardId.VW_Jiji);
                    } else if (settable == 0 && Bot.HasInMonstersZone(CardId.VW_Lulu)) {
                        AI.SelectCard(CardId.VW_Lulu);
                        AI.SelectNextCard(CardId.VWGate_Qinglong);
                        AI.SelectNextCard(CardId.VW_Lili);
                    } else {
                        return false;
                    }
                    oncePerTurn[CardId.VW_Laolao] = true;
                    return true;
                } else if (knownComboPaths["Jiji+City"] && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VW_NiangNiang);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    oncePerTurn[CardId.VW_Laolao] = true;
                    return true;
                } else if (knownComboPaths["Lao+City"] && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    // AI.SelectNextCard(CardId.VW_Jiji);
                    oncePerTurn[CardId.VW_Laolao] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Niang"] && Bot.HasInMonstersZone(CardId.VW_JiuJiu)) {
                    AI.SelectCard(CardId.VW_JiuJiu);
                    AI.SelectNextCard(CardId.VWGate_Xuanwu);
                    AI.SelectNextCard(CardId.VW_NiangNiang);
                    oncePerTurn[CardId.VW_Laolao] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Jiji"] && Bot.HasInMonstersZone(CardId.VW_JiuJiu)) {
                    AI.SelectCard(CardId.VW_JiuJiu);
                    AI.SelectNextCard(CardId.VWGate_Xuanwu);
                    AI.SelectNextCard(CardId.VW_Lulu);
                    oncePerTurn[CardId.VW_Laolao] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Lao"] && Bot.HasInMonstersZone(CardId.VW_Lulu)) {
                    AI.SelectCard(CardId.VW_Lulu);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    AI.SelectYesNo(false);
                    oncePerTurn[CardId.VW_Laolao] = true;
                    return true;
                } else if (knownComboPaths["Jiji+Lao"] && Bot.HasInMonstersZone(CardId.VW_Jiji)) {
                    AI.SelectCard(CardId.VW_Jiji);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    // AI.SelectYesNo(false);
                    oncePerTurn[CardId.VW_Laolao] = true;
                    return true;
                } else if (knownComboPaths["Niang+Lao"] && Bot.HasInMonstersZone(CardId.VW_NiangNiang)) {
                    AI.SelectCard(CardId.VW_NiangNiang);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    // AI.SelectYesNo(false);
                    oncePerTurn[CardId.VW_Laolao] = true;
                    return true;
                } else if (knownComboPaths["other"]) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VW_NiangNiang);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    oncePerTurn[CardId.VW_Laolao] = true;
                    return true;
                }
            }
            return false;
        }

        private bool VW_Lili()
        {
            if (!oncePerTurn.ContainsKey(CardId.VW_Lili)) {
                if (knownComboPaths["Lulu+City"] && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    oncePerTurn[CardId.VW_Lili] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Qing"] && Bot.HasInSpellZone(CardId.VWGate_Qinglong)) {
                    AI.SelectCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VW_Laolao);
                    AI.SelectNextCard(CardId.VWGate_Chuche);
                    oncePerTurn[CardId.VW_Lili] = true;
                    return true;
                } else if (knownComboPaths["Lili+City"] && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VW_NiangNiang);
                    oncePerTurn[CardId.VW_Lili] = true;
                    return true;
                } else if (knownComboPaths["Lili+Qing"] && Bot.HasInSpellZone(CardId.VWGate_Qinglong)) {
                    AI.SelectCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VWGate_Xuanwu);
                    AI.SelectNextCard(CardId.VW_Lulu);
                    oncePerTurn[CardId.VW_Lili] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Lili"] && Bot.HasInMonstersZone(CardId.VW_Lulu)) {
                    AI.SelectCard(CardId.VW_Lulu);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    AI.SelectNextCard(CardId.VWGate_Xuanwu);
                    oncePerTurn[CardId.VW_Lili] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Lao"] && Bot.HasInMonstersZone(CardId.VW_Laolao)) {
                    AI.SelectCard(CardId.VW_Laolao);
                    AI.SelectNextCard(CardId.VWGate_Xuanwu);
                    AI.SelectYesNo(true);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    oncePerTurn[CardId.VW_Lili] = true;
                    return true;
                } else if (knownComboPaths["other"]) {
                    AI.SelectCard(CardId.VW_Lulu);

                    if (!Bot.HasInGraveyard(CardId.VWGate_Qinglong)) {
                        AI.SelectNextCard(CardId.VWGate_Qinglong);
                    }
                    if (!Bot.HasInGraveyard(CardId.VWGate_Xuanwu)) {
                        AI.SelectNextCard(CardId.VWGate_Xuanwu);
                    }
                    if (!Bot.HasInGraveyard(CardId.VW_NiangNiang)) {
                        AI.SelectNextCard(CardId.VW_NiangNiang);
                    }
                    if (!Bot.HasInGraveyard(CardId.VW_Toutou)) {
                        AI.SelectNextCard(CardId.VW_Toutou);
                    }
                    if (!Bot.HasInGraveyard(CardId.VW_Jiji)) {
                        AI.SelectNextCard(CardId.VW_Jiji);
                    }
                    oncePerTurn[CardId.VW_Lili] = true;
                    return true;
                }
            }
            return false;
        }

        private bool VW_Jiji()
        {
            if (!oncePerTurn.ContainsKey(CardId.VW_Jiji)) {
                if (knownComboPaths["Lulu+City"] && Bot.HasInMonstersZone(CardId.VW_Laolao)) {
                    AI.SelectCard(CardId.VW_Laolao);
                    AI.SelectNextCard(CardId.VWGate_Xuanwu);
                    oncePerTurn[CardId.VW_Jiji] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Qing"] && Bot.HasInMonstersZone(CardId.VW_Lulu)) {
                    AI.SelectCard(CardId.VW_Lulu);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    oncePerTurn[CardId.VW_Jiji] = true;
                    return true;
                } else if (knownComboPaths["Lili+City"] && Bot.HasInMonstersZone(CardId.VW_NiangNiang)) {
                    AI.SelectCard(CardId.VW_NiangNiang);
                    AI.SelectNextCard(CardId.VWGate_Xuanwu);
                    oncePerTurn[CardId.VW_Jiji] = true;
                    return true;
                } else if (knownComboPaths["Lili+Qing"]) {
                    if (settable != 0 && Bot.HasInMonstersZone(CardId.VW_Lulu)) {
                        AI.SelectCard(CardId.VW_Lulu);
                        AI.SelectNextCard(CardId.VWGate_Qinglong);
                    } else if (settable == 0 && Bot.HasInSpellZone(CardId.VWGate_Qinglong)) {
                        AI.SelectCard(CardId.VWGate_Qinglong);
                        AI.SelectNextCard(CardId.VW_NiangNiang);
                    } else {
                        return false;
                    }
                    oncePerTurn[CardId.VW_Jiji] = true;
                    return true;
                } else if (knownComboPaths["Jiji+City"] && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    oncePerTurn[CardId.VW_Jiji] = true;
                    return true;
                } else if (knownComboPaths["Lao+City"] && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VW_NiangNiang);
                    oncePerTurn[CardId.VW_Jiji] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Lulu"] && Bot.HasInMonstersZone(CardId.VW_Lulu)) {
                    AI.SelectCard(CardId.VW_Lulu);
                    AI.SelectNextCard(CardId.VWGate_Chuche);
                    oncePerTurn[CardId.VW_Jiji] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Lao"] && Bot.HasInMonstersZone(CardId.VW_Laolao)) {
                    AI.SelectCard(CardId.VW_Laolao);
                    AI.SelectNextCard(CardId.VWGate_Xuanwu);
                    oncePerTurn[CardId.VW_Jiji] = true;
                    return true;
                } else if (knownComboPaths["Jiji+Lao"] && Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    AI.SelectCard(CardId.VWGate_Chuche);
                    AI.SelectNextCard(CardId.VWGate_Qinglong);
                    oncePerTurn[CardId.VW_Jiji] = true;
                    return true;
                } else if (knownComboPaths["other"]) {
                    AI.SelectCard(CardId.VW_Lulu);

                    if (!Bot.HasInGraveyard(CardId.VWGate_Qinglong)) {
                        AI.SelectNextCard(CardId.VWGate_Qinglong);
                    } else if (!Bot.HasInGraveyard(CardId.VWGate_Xuanwu)) {
                        AI.SelectNextCard(CardId.VWGate_Xuanwu);
                    } else if (!Bot.HasInGraveyard(CardId.VWGate_Chuche)) {
                        AI.SelectNextCard(CardId.VWGate_Chuche);
                    }

                    oncePerTurn[CardId.VW_Jiji] = true;
                    return true;
                }
            }

            if (BotDuel.Phase == DuelPhase.End) {
                if (!Bot.HasInHand(CardId.VW_Lulu)) {
                    AI.SelectCard(CardId.VW_Lulu);
                } else {
                    AI.SelectCard(CardId.VW_Laolao);
                }
                return true;
            }

            if (knownComboPaths["Lili+Qing"] && oncePerTurn.ContainsKey(CardId.VW_Jiji)) {
                return true;
            } else if (knownComboPaths["Lao+City"] && oncePerTurn.ContainsKey(CardId.VW_Jiji)) {
                return true;
            } else if (knownComboPaths["Lulu+Jiji"] && !Bot.HasInMonstersZone(CardId.VW_Jiji)) {
                return true;
            } else if (knownComboPaths["Jiji+Lao"] && !Bot.HasInMonstersZone(CardId.VW_Jiji)) {
                return true;
            } else if (knownComboPaths["other"]) {
                return true;
            }
            
            return false;
        }

        private bool VWCity_Kauwloon()
        {
            if (!Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                AI.SelectCard(CardId.VWGate_Chuche);
                return true;
            } else if (!Bot.HasInSpellZone(CardId.VWGate_Qinglong)) {
                AI.SelectCard(CardId.VWGate_Qinglong);
                return true;
            }
            return false;
        }

        private bool VWGate_QinglongNegateEff()
        {
            if (DynamicCard.Location == CardLocation.SpellZone) {
                if (Bot.HasInMonstersZone(CardId.InvokedCaliga)) {
                    AI.SelectCard(CardId.VWCity_Kauwloon);
                    AI.SelectNextCard(CardId.InvokedCaliga);
                    return true;
                } else if (Util.GetProblematicEnemyMonster(0, true) != null) {
                    AI.SelectCard(CardId.VWCity_Kauwloon);
                    AI.SelectNextCard(Util.GetProblematicEnemyMonster(0, true));
                    return true;
                }
                return false;
            }
            return false;
        }

        private bool VWGate_Qinglong()
        {
            if (DynamicCard.Location == CardLocation.Grave) {
                if (
                    knownComboPaths["Lulu+City"]  ||
                    knownComboPaths["Lulu+Niang"] ||
                    knownComboPaths["Lulu+Jiji"]  ||
                    (knownComboPaths["Lili+Qing"] && settable != 0)
                ) {
                    AI.SelectCard(CardId.VW_Laolao);
                    AI.SelectNextCard(dumpFromHand(CardId.VW_Laolao));
                    oncePerTurn[CardId.VWGate_Qinglong] = true;
                    return true;
                } else if (
                    knownComboPaths["Lulu+Qing"] ||
                    (knownComboPaths["Lulu+Lao"] && settable != 0)
                ) {
                    AI.SelectCard(CardId.VW_Lili);
                    AI.SelectNextCard(dumpFromHand(CardId.VW_Lili));
                    oncePerTurn[CardId.VWGate_Qinglong] = true;
                    return true;
                } else if (
                    knownComboPaths["Lili+City"] ||
                    knownComboPaths["Jiji+City"] ||
                    knownComboPaths["Lao+City"]  ||
                    knownComboPaths["Lulu+Lili"] ||
                    knownComboPaths["Jiji+Lao"]  ||
                    knownComboPaths["Niang+Lao"]
                ) {
                    AI.SelectCard(CardId.VW_Lulu);
                    AI.SelectNextCard(dumpFromHand(CardId.VW_Lulu));
                    oncePerTurn[CardId.VWGate_Qinglong] = true;
                    return true;
                } else if (
                    knownComboPaths["Lulu+Lulu"] ||
                    (knownComboPaths["Lili+Qing"] && settable == 0) ||
                    (knownComboPaths["Lulu+Lao"] && settable == 0)
                ) {
                    AI.SelectCard(CardId.VW_Jiji);
                    AI.SelectNextCard(dumpFromHand(CardId.VW_Jiji));
                    oncePerTurn[CardId.VWGate_Qinglong] = true;
                    return true;
                } else if (knownComboPaths["other"]) {
                    if (!Bot.HasInHand(CardId.VW_Lulu)) {
                        AI.SelectCard(CardId.VW_Lulu);
                        AI.SelectNextCard(dumpFromHand(CardId.VW_Lulu));
                    } else if (!Bot.HasInHand(CardId.VW_Jiji)) {
                        AI.SelectCard(CardId.VW_Jiji);
                        AI.SelectNextCard(dumpFromHand(CardId.VW_Jiji));
                    } else if (!Bot.HasInHand(CardId.VW_Laolao)) {
                        AI.SelectCard(CardId.VW_Laolao);
                        AI.SelectNextCard(dumpFromHand(CardId.VW_Laolao));
                    } else {
                        AI.SelectCard(CardId.VW_Lulu);
                        AI.SelectNextCard(dumpFromHand(CardId.VW_Lulu));
                    }
                    oncePerTurn[CardId.VWGate_Qinglong] = true;
                    return true;
                } else {
                    return false;
                }
            }

            if (DynamicCard.Location == CardLocation.Hand) {
                if (!Bot.HasInSpellZone(CardId.VWGate_Qinglong) && (
                        knownComboPaths["Lulu+Qing"] ||
                        knownComboPaths["Lili+Qing"]
                )) {
                    return true;
                } else if (knownComboPaths["other"] && !Bot.HasInSpellZone(new List<int> {
                        CardId.VWGate_Chuche,
                        CardId.VWGate_Qinglong,
                        CardId.VWGate_Xuanwu
                    })) {
                        return true;
                }
            }
            
            return false;
        }

        private bool VWGate_ChuchePopEff()
        {
            if (DynamicCard.Location == CardLocation.SpellZone) {
                if (Util.GetBestEnemyCard(true, true) != null) {
                    AI.SelectCard(Util.GetBestEnemyCard(true, true));
                    return true;
                }
                return false;
            }
            return false;
        }

        private bool VWGate_Chuche()
        {
            if (DynamicCard.Location == CardLocation.Grave) {
                if (knownComboPaths["Lili+Qing"] && settable == 0 && Bot.HasInMonstersZone(CardId.DarkDragoon)) {
                    AI.SelectCard(CardId.VW_Jiji);
                    AI.SelectOption(1);
                    oncePerTurn[CardId.VWGate_Chuche] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Lulu"] && Bot.HasInMonstersZone(CardId.VW_Shenshen)) {
                    AI.SelectCard(CardId.VW_Shenshen);
                    AI.SelectOption(1);
                    oncePerTurn[CardId.VWGate_Chuche] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Lili"] && Bot.HasInMonstersZone(CardId.VW_Lulu)) {
                    AI.SelectCard(CardId.VW_Lulu);
                    AI.SelectOption(1);
                    oncePerTurn[CardId.VWGate_Chuche] = true;
                    return true;
                } else if (Bot.HasInSpellZone(CardId.VWGate_Chuche)) {
                    //todo: banish for chuche eff if removed < 2
                    //todo: banish for gain eff if !(level == 6 OR level == 9) in extra
                    return true;
                } else {
                    return false;
                }
            } 
            return false;
        }

        private bool VWGate_XuanwuReposEff()
        {
            if (DynamicCard.Location == CardLocation.SpellZone) {
                if (BotDuel.Phase == DuelPhase.BattleStart) {
                    AI.SelectCard(Enemy.MonsterZone.GetHighestDefenseMonster(true));
                    return true;
                }
                return false;
            }
            return false;
        }

        private bool VWGate_Xuanwu()
        {
            if (DynamicCard.Location == CardLocation.Grave) {
                if (
                    knownComboPaths["Lulu+Qing"]
                ) {
                    //todo: improve this
                    if (settable != 0 && Bot.HasInGraveyard(CardId.VW_Lili) && oncePerTurn.ContainsKey(CardId.VW_Laolao)) {
                        AI.SelectCard(CardId.VW_Lili);
                        AI.SelectNextCard(dumpFromHand(CardId.VW_Laolao));
                        return true;
                    } else if (settable == 0 && Bot.HasInGraveyard(CardId.VW_Jiji) && Bot.HasInMonstersZone(CardId.DarkDragoon)) {
                        AI.SelectCard(CardId.VW_Jiji);
                        AI.SelectNextCard(dumpFromHand(0));
                        return true;
                    } else {
                        return false;
                    }
                } else if (knownComboPaths["Lili+City"] && oncePerTurn.ContainsKey(CardId.UltimayaTzolkin)) {
                    AI.SelectCard(CardId.VW_NiangNiang);
                    AI.SelectNextCard(dumpFromHand(0));
                    return true;
                } else if (
                    Bot.HasInGraveyard(CardId.VW_Lulu) && (
                    knownComboPaths["Lili+Qing"]  ||
                    knownComboPaths["Lulu+Niang"] ||
                    knownComboPaths["Jiji+Lao"]
                )) {
                    AI.SelectCard(CardId.VW_Lulu);
                    AI.SelectNextCard(dumpFromHand(0));
                    return true;
                } else if (
                    Bot.HasInGraveyard(CardId.VW_Lili) && (
                    knownComboPaths["Lulu+Lili"] ||
                    (knownComboPaths["Lulu+Lao"] && oncePerTurn.ContainsKey(CardId.UltimayaTzolkin)) ||
                    (knownComboPaths["Lulu+City"] && settable != 0 && oncePerTurn.ContainsKey(CardId.VW_Jiji))
                )) {
                    AI.SelectCard(CardId.VW_Lili);
                    AI.SelectNextCard(dumpFromHand(0));
                    return true;
                } else if (
                    Bot.HasInGraveyard(CardId.VW_Jiji) && (
                    knownComboPaths["Lulu+Jiji"] ||
                    (knownComboPaths["Lulu+Lao"] && oncePerTurn.ContainsKey(CardId.ConstellarPtolemyM7)) ||
                    (knownComboPaths["Lulu+City"] && settable == 0 && oncePerTurn.ContainsKey(CardId.VW_Jiji))
                )) {
                    AI.SelectCard(CardId.VW_Jiji);
                    AI.SelectNextCard(dumpFromHand(0));
                    return true;
                }
            }
            return false;
        }
        private bool EmergencyTeleport()
        {
            if (
                knownComboPaths["Lulu+Lulu"] ||
                knownComboPaths["Lulu+Lili"] ||
                knownComboPaths["Lulu+Lao"]
            ) {
                AI.SelectCard(CardId.VW_Lulu);
                normalSummoned[CardId.VW_Lulu] = true;
                return true;
            } else if (
                knownComboPaths["Lulu+Niang"] ||
                knownComboPaths["Niang+Lao"]
            ) {
                AI.SelectCard(CardId.VW_NiangNiang);
                normalSummoned[CardId.VW_NiangNiang] = true;
                return true;
            } else if (knownComboPaths["other"] && BotDuel.Phase == DuelPhase.Main1) {
                if (Bot.HasInMonstersZone(CardId.StardustChargeWarrior)) {
                    AI.SelectCard(CardId.PSYFramegearGamma);
                    return true;
                } else if (Bot.GetMonsters().Count(card => card.IsTuner()) > 0) {
                    AI.SelectCard(CardId.VW_NiangNiang);
                    return true;
                } else if (Bot.GetMonsters().Count(card => !card.IsTuner()) > 0) {
                    AI.SelectCard(CardId.VW_Lulu);
                    return true;
                }
                AI.SelectCard(CardId.VW_NiangNiang);
                return true;
            } else {
                return false;
            }
        }
        private bool PotOfDesires()
        {
            if (knownComboPaths.Where(path => path.Key != "other").Count(path => path.Value == true) > 0) {
                return false;
            } else {
                //todo: decide if activate on main 1 or main 2
                oncePerTurn[CardId.PotOfDesires] = true;
                return DefaultPotOfDesires();
            }
        }

        private bool PotOfProsperity()
        {
            if (knownComboPaths.Where(path => path.Key != "other").Count(path => path.Value == true) > 0) {
                return false;
            } else if (!Bot.HasInMonstersZone(new List<int>() {
                CardId.CrystalWingSynchroDragon,
                CardId.DarkDragoon
            })) {
                AI.SelectCard(CardId.CoralDragon);
                AI.SelectCard(CardId.DivineArsenalZEUS);
                AI.SelectCard(CardId.FA_DawnDragster);
                // todo: dig for 6 and calculate proximity to $path
                if (Bot.HasInHand(spellTargets)) {
                    AI.SelectCard(monsterTargets);
                } else if (Bot.HasInHand(monsterTargets)) {
                    AI.SelectCard(spellTargets.Concat(monsterTargets).ToList());
                } else {
                    AI.SelectCard(handTraps);
                }
                oncePerTurn[CardId.PotOfProsperity] = true;
                return true;
            }
            return false;
        }
            
        // extra
        private bool StardustChargeWarrior()
        {
            if (Bot.HasInMonstersZone(CardId.StardustChargeWarrior)) {
                return true;
            } else {
                if (knownComboPaths["Lulu+City"] && Bot.HasInMonstersZone(CardId.VW_NiangNiang) && Bot.HasInMonstersZone(CardId.VW_Jiji)) {
                    AI.SelectCard(CardId.VW_NiangNiang);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    return true;
                } else if (
                    Bot.HasInMonstersZone(CardId.VW_Lulu) && Bot.HasInMonstersZone(CardId.VW_Jiji) && (
                    knownComboPaths["Lulu+Qing"] ||
                    (knownComboPaths["Lili+Qing"] && oncePerTurn.ContainsKey(CardId.VermillionDragonMech)) ||
                    knownComboPaths["Jiji+City"] ||
                    knownComboPaths["Lao+City"]  ||
                    knownComboPaths["Lulu+Lulu"] ||
                    knownComboPaths["Lulu+Lao"]  ||
                    knownComboPaths["Jiji+Lao"]
                )) {
                    AI.SelectCard(CardId.VW_Lulu);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    return true;
                } else if (knownComboPaths["other"]) {
                    return true;
                }
            }
            return false;
        }

        private bool CoralDragon()
        {
            if (Bot.HasInMonstersZone(CardId.CoralDragon)) {
                // if turn +1 and valid target in field -> eff
                if (Util.GetProblematicEnemyCard(1, true) != null) {
                    AI.SelectCard(dumpFromHand(0));
                    AI.SelectNextCard(Util.GetProblematicEnemyCard(1, true));
                    return true;
                }
                return false;
            } else {
                if (knownComboPaths["other"]) {
                    return true;
                }
            }
            
            // draw eff
            if (DynamicCard.Location == CardLocation.Grave) {
                return true;
            }
            return false;
        }
        private bool GaiaDragon()
        {
            if (
                (knownComboPaths["Lulu+City"] && settable == 0) ||
                knownComboPaths["Lulu+Qing"] || 
                knownComboPaths["Lili+City"] ||
                knownComboPaths["Lili+Qing"] ||
                knownComboPaths["Jiji+City"] ||
                knownComboPaths["Lao+City"]  ||
                knownComboPaths["Lulu+Lao"]  ||
                knownComboPaths["Jiji+Lao"]
            ) {
                AI.SelectCard(CardId.ConstellarPtolemyM7);
                return true;
            } else if (
                knownComboPaths["other"] &&
                Bot.HasInMonstersZone(CardId.ConstellarPtolemyM7) &&
                !Bot.HasInMonstersZone(CardId.ConstellarPtolemyM7, false, false)
            ) {
                // todo: test this
                return true;
            }
            return false;
        }
        private bool DarkDragoon()
        {
            if (BotDuel.LastChainPlayer == 1) {
                AI.SelectCard(dumpFromHand(0));
                return true;
            }
            return false;
        }
        private bool VermillionDragonMech()
        {
            if (knownComboPaths["Lili+Qing"]) {
                AI.SelectCard(CardId.VW_Lulu);
                AI.SelectNextCard(CardId.VW_Lili);
                return true;
            } else if (knownComboPaths["other"]) {
                // summon vermillion before shenshen to possibly bait negates
                if (!Bot.HasInExtra(CardId.VW_Shenshen) && Bot.HasInExtra(CardId.VermillionDragonMech)) {
                    return true;
                }
            }

            if (Bot.HasInMonstersZone(CardId.VermillionDragonMech)) {
                if (knownComboPaths["Lili+Qing"]) {
                    // eff to pop himself as part of lili + qing combo
                    AI.SelectCard(CardId.VW_Lulu);
                    AI.SelectCard(CardId.VermillionDragonMech);
                    return true;
                } else if (Util.GetBestEnemyCard(false, true) != null && Bot.Graveyard.Any(card => card.IsTuner())) {
                    AI.SelectCard(Bot.Graveyard.Where(card => card.IsTuner()).FirstOrDefault());
                    AI.SelectNextCard(Util.GetBestEnemyCard(false, true));
                    return true;
                }
            }
                
            if (Bot.HasInGraveyard(CardId.VermillionDragonMech)) {
                if (knownComboPaths["Lili+Qing"]) {
                    AI.SelectCard(CardId.VW_Lulu);
                } else if(knownComboPaths["other"]) {
                    if (Bot.HasInBanished(CardId.VW_Lulu)) {
                        AI.SelectCard(CardId.VW_Lulu);
                    } else if (Bot.HasInBanished(CardId.VW_Laolao)) {
                        AI.SelectCard(CardId.VW_Laolao);
                    }
                }
                return true;
            }

            return false;
        }
        private bool VW_Shenshen()
        {
            if (Util.IsTurn1OrMain2()) {
                AI.SelectPosition(CardPosition.FaceUpDefence);
            } else {
                AI.SelectPosition(CardPosition.FaceUpAttack);
            }

            if (
                knownComboPaths["Lulu+City"] || 
                (knownComboPaths["Lulu+Qing"] && settable == 0)
            ) {
                AI.SelectCard(CardId.VW_Laolao);
                AI.SelectNextCard(CardId.VW_Jiji);
                return true;
            } else if (
                (knownComboPaths["Lili+City"] && settable == 0) ||
                knownComboPaths["Lulu+Lili"] ||
                knownComboPaths["Lulu+Lao"]
            ) {
                AI.SelectCard(CardId.VW_Lulu);
                AI.SelectNextCard(CardId.VW_Lili);
                return true;
            } else if (knownComboPaths["Lili+Qing"]) {
                AI.SelectCard(CardId.VW_NiangNiang);
                AI.SelectNextCard(CardId.VW_Jiji);
                return true;
            } else if (knownComboPaths["Lulu+Lulu"]) {
                AI.SelectCard(CardId.VW_Lulu);
                AI.SelectNextCard(CardId.StardustChargeWarrior);
                return true;
            } else if (knownComboPaths["Niang+Lao"]) {
                AI.SelectCard(CardId.VW_Laolao);
                AI.SelectNextCard(CardId.VW_NiangNiang);
                return true;
            } else if (knownComboPaths["other"]) {
                return true;
            }

            if (Bot.HasInMonstersZoneOrInGraveyard(CardId.VW_Shenshen)) {
                AI.SelectPosition(CardPosition.FaceUpAttack);
                return true;
            }

            return false;
        }

        private bool VW_JiuJiu() 
        {
            if (Bot.HasInMonstersZone(CardId.VW_JiuJiu)) {
                if (Util.GetBestEnemyCard(false, true) != null) {
                    AI.SelectCard(Util.GetBestEnemyCard(false, true));
                    return true;
                }
            }

            if (Util.IsTurn1OrMain2()) {
                AI.SelectPosition(CardPosition.FaceUpDefence);
            } else {
                AI.SelectPosition(CardPosition.FaceUpAttack);
            }

            if (knownComboPaths["Lulu+Niang"]) {
                AI.SelectCard(CardId.VW_Lulu);
                AI.SelectNextCard(CardId.VW_NiangNiang);
                return true;
            } else if (knownComboPaths["Lulu+Jiji"]) {
                AI.SelectCard(CardId.VW_Lulu);
                AI.SelectNextCard(CardId.VW_Jiji);
                return true;
            } else if (knownComboPaths["other"]) {
                if(!Bot.HasInExtra(new List<int>() {
                    CardId.StardustChargeWarrior,
                    CardId.CoralDragon
                })) {
                    return true;
                }
            }
            return false;
        }

        private bool UltimayaTzolkin()
        {
            if (Bot.HasInMonstersZone(CardId.UltimayaTzolkin)) {
                AI.SelectCard(CardId.CrystalWingSynchroDragon);
                oncePerTurn[CardId.UltimayaTzolkin] = true;
                return true;
            }

            if (
                (knownComboPaths["Lulu+City"] && settable != 0) ||
                knownComboPaths["Lulu+Qing"] ||
                knownComboPaths["Lili+City"] ||
                knownComboPaths["Lulu+Lao"]
            ) {
                AI.SelectPosition(CardPosition.FaceUpDefence);
                AI.SelectCard(CardId.VW_Laolao);
                AI.SelectNextCard(CardId.VW_Lili);
                return true;
            } else if (
                knownComboPaths["Lili+Qing"] ||
                knownComboPaths["Jiji+City"]
            ) {
                AI.SelectPosition(CardPosition.FaceUpDefence);
                AI.SelectCard(CardId.VW_Laolao);
                AI.SelectNextCard(CardId.StardustChargeWarrior);
                return true;
            } else if (
                knownComboPaths["Lulu+Niang"] ||
                knownComboPaths["Lulu+Jiji"]
            ) {
                AI.SelectPosition(CardPosition.FaceUpDefence);
                AI.SelectCard(CardId.VW_Laolao);
                AI.SelectNextCard(CardId.VW_JiuJiu);
                return true;
            } else if (knownComboPaths["Lulu+Lili"]) {
                AI.SelectPosition(CardPosition.FaceUpDefence);
                AI.SelectCard(CardId.VW_Lulu);
                AI.SelectNextCard(CardId.VW_Lili);
                return true;
            } else if (knownComboPaths["other"] && settable != 0) {
                AI.SelectPosition(CardPosition.FaceUpDefence);
                return true;
            }
            return false;
        }

        private bool MuddyMudragon()
        {
            if (Bot.HasInMonstersZone(CardId.MuddyMudragon)) {
                if (Bot.HasInMonstersZone(CardId.UltimayaTzolkin)) {
                    AI.SelectPosition(CardPosition.FaceUpDefence);  
                    AI.SelectCard(CardId.InvokedCaliga);
                    AI.SelectNextCard(CardId.MuddyMudragon);
                    AI.SelectNextCard(CardId.UltimayaTzolkin);
                    return true;
                } else if (Bot.HasInMonstersZone(CardId.GaiaDragon)) {
                    AI.SelectPosition(CardPosition.FaceUpAttack);
                    AI.SelectCard(CardId.DarkDragoon);
                    AI.SelectNextCard(CardId.MuddyMudragon);
                    AI.SelectNextCard(CardId.GaiaDragon);
                    return true;
                }
            } else {
                if (
                    knownComboPaths["Lulu+City"] ||
                    knownComboPaths["Lulu+Jiji"] ||
                    knownComboPaths["Lulu+Lao"]  ||
                    knownComboPaths["Jiji+Lao"]  ||
                    (knownComboPaths["Lili+Qing"] && settable == 0)
                ) {
                    AI.SelectPosition(CardPosition.FaceUpDefence);
                    AI.SelectCard(CardId.VW_Lulu);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    return true;
                } else if (
                    knownComboPaths["Lulu+Qing"] ||
                    knownComboPaths["Jiji+City"] ||
                    knownComboPaths["Lao+City"]  ||
                    (knownComboPaths["Lili+Qing"] && settable != 0) ||
                    (knownComboPaths["Lili+City"] && settable == 0)
                ) {
                    AI.SelectPosition(CardPosition.FaceUpDefence);
                    AI.SelectCard(CardId.VW_NiangNiang);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    return true;
                } else if (
                    knownComboPaths["Lulu+Niang"] ||
                    (knownComboPaths["Lili+City"] && settable != 0)
                ) {
                    AI.SelectPosition(CardPosition.FaceUpDefence);
                    AI.SelectCard(CardId.VW_Lulu);
                    AI.SelectNextCard(CardId.VW_NiangNiang);
                    return true;
                } else if (knownComboPaths["other"] && Bot.HasInMonstersZone(CardId.UltimayaTzolkin)) {
                    AI.SelectPosition(CardPosition.FaceUpDefence);
                    return true;
                }
            }
            return false;
        }

        private bool CrystalWingSynchroDragon()
        {
            AI.SelectPosition(CardPosition.FaceUpAttack);
            return BotDuel.LastChainPlayer == 1;
        }

        public bool DivineArsenalZEUS()
        {
            if (Bot.HasInExtra(CardId.DivineArsenalZEUS)) {
                return true;
            }

            if (
                Bot.HasInMonstersZone(new List<int>() {
                    CardId.DarkDragoon,
                    CardId.CrystalWingSynchroDragon,
                    CardId.InvokedCaliga
                }, true)
            ) {
                return false;
            } else if (BotDuel.Player != 0) {
                return Bot.GetFieldCount() < Enemy.GetFieldCount();
            }

            return false;
        }

        public bool Number39UtopiaBeyond()
        {
            if (Bot.HasInMonstersZone(CardId.Number39UtopiaBeyond)) {
                return true;
            } else {
                if (
                    (Enemy.HasAttackingMonster() &&
                    Util.GetTotalAttackingMonsterAttack(0) >= Enemy.LifePoints - 3000) ||
                    Enemy.GetMonsters().Any(card => card.Attack >= 2700) ||
                    Util.GetProblematicEnemyMonster(0, false) != null
                ) {
                    return BotDuel.Turn > 1;
                }
                return false;
            }
        }
        public bool ConstellarPtolemyM7()
        {
            if (Bot.HasInMonstersZone(CardId.ConstellarPtolemyM7)) {
                AI.SelectOption(1);
                if (
                    Bot.HasInGraveyard(CardId.VW_Jiji) && (
                    knownComboPaths["Lulu+City"] || 
                    knownComboPaths["Lili+City"] || 
                    knownComboPaths["Lili+Qing"]
                )) {
                    AI.SelectCard(CardId.VW_Lili);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    oncePerTurn[CardId.ConstellarPtolemyM7] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Qing"] && Bot.HasInGraveyard(CardId.VW_Laolao)) {
                    AI.SelectCard(CardId.VW_Lili);
                    AI.SelectNextCard(CardId.VW_Laolao);
                    oncePerTurn[CardId.ConstellarPtolemyM7] = true;
                    return true;
                } else if (knownComboPaths["Jiji+City"] && Bot.HasInGraveyard(CardId.VW_Lulu)) {
                    AI.SelectCard(CardId.VW_Laolao);
                    AI.SelectNextCard(CardId.VW_Lulu);
                    oncePerTurn[CardId.ConstellarPtolemyM7] = true;
                    return true;
                } else if (knownComboPaths["Lao+City"] && Bot.HasInGraveyard(CardId.VW_Jiji)) {
                    AI.SelectCard(CardId.VW_Laolao);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    oncePerTurn[CardId.ConstellarPtolemyM7] = true;
                    return true;
                } else if (knownComboPaths["Lulu+Lao"] && settable == 0 && Bot.HasInGraveyard(CardId.VW_Lulu)) {
                    AI.SelectCard(CardId.VW_Laolao);
                    AI.SelectNextCard(CardId.VW_Lulu);
                    oncePerTurn[CardId.ConstellarPtolemyM7] = true;
                    return true;
                } else if (knownComboPaths["Jiji+Lao"] && Bot.HasInGraveyard(CardId.VW_Jiji)) {
                    AI.SelectCard(CardId.VW_Laolao);
                    AI.SelectNextCard(CardId.VW_Jiji);
                    oncePerTurn[CardId.ConstellarPtolemyM7] = true;
                    return true;
                } else if (knownComboPaths["other"]) {
                    if (
                        Bot.HasInGraveyard(CardId.MuddyMudragon) && (
                            Bot.HasInExtra(CardId.DarkDragoon) ||
                            Bot.HasInExtra(CardId.UltimayaTzolkin)
                        )
                    ) {
                        AI.SelectCard(0);
                        AI.SelectNextCard(CardId.MuddyMudragon);
                        oncePerTurn[CardId.ConstellarPtolemyM7] = true;
                        return true;
                    } else if (Bot.HasInGraveyard(monsterTargets)) {
                        AI.SelectCard(0);
                        AI.SelectNextCard(monsterTargets);
                        oncePerTurn[CardId.ConstellarPtolemyM7] = true;
                        return true;
                    } else if (Bot.HasInGraveyard(handTraps)) {
                        AI.SelectCard(0);
                        AI.SelectNextCard(handTraps);
                        oncePerTurn[CardId.ConstellarPtolemyM7] = true;
                        return true;
                    }
                    return false;
                }
            } else {
                if (Util.IsTurn1OrMain2()) {
                    AI.SelectPosition(CardPosition.FaceUpDefence);
                } else {
                    AI.SelectPosition(CardPosition.FaceUpAttack);
                }

                if (
                    knownComboPaths["Lulu+City"] ||
                    knownComboPaths["Lulu+Qing"]) {
                    AI.SelectCard(CardId.VW_Lili);
                    AI.SelectCard(CardId.StardustChargeWarrior);
                    return true;
                } else if (knownComboPaths["Lili+City"] || knownComboPaths["Lili+Qing"]) {
                    AI.SelectCard(CardId.VW_Lili);
                    AI.SelectCard(CardId.VW_Laolao);
                    return true;
                } else if (
                    knownComboPaths["Jiji+City"] ||
                    knownComboPaths["Lao+City"]  ||
                    knownComboPaths["Lulu+Lao"]  ||
                    knownComboPaths["Jiji+Lao"]
                ) {
                    AI.SelectCard(CardId.StardustChargeWarrior);
                    AI.SelectCard(CardId.VW_Laolao);
                    return true;
                } else if (knownComboPaths["other"]) {
                    return BotDuel.Turn == 1 || !Bot.HasInExtra(CardId.StardustChargeWarrior);
                }
            }
            return false;
        }

        private bool MonsterRepos()
        {
            return (
                DynamicCard.IsCode(CardId.DarkDragoon) ||
                DynamicCard.IsCode(CardId.CrystalWingSynchroDragon)
            ) && DynamicCard.IsAttack() ? false : DefaultMonsterRepos();
        }

        private bool SpellSet()
        {
            return (
                (DynamicCard.IsCode(CardId.VWGate_Chuche) && !Bot.HasInSpellZone(CardId.VWGate_Chuche)) ||
                DynamicCard.IsCode(CardId.InfiniteImpermanence)
            ) && (BotDuel.Phase != DuelPhase.Main2 || !Bot.HasInMonstersZone(CardId.UltimayaTzolkin)) ? false : DefaultSpellSet();
        }

        private int TzolkinWinCon()
        {
            List<string> activePath = knownComboPaths
                .Where(path => path.Value == true)
                .Select(path => path.Key)
                .ToList();

            bool pathUsesCity = activePath.Any(path => Regex.IsMatch(path, Regex.Escape("*City*").Replace(@"\*", ".*").Replace(@"\?", ".")));
            bool pathUsesQing = activePath.Any(path => Regex.IsMatch(path, Regex.Escape("*Qing*").Replace(@"\*", ".*").Replace(@"\?", ".")));

            foreach (BotClientCard card in Bot.Hand) {
                switch (card.Id) {
                    case CardId.InfiniteImpermanence: return card.Id;
                    case CardId.PotOfDesires: return card.Id;
                    case CardId.VWCity_Kauwloon: if(!pathUsesCity) return card.Id; break;
                    case CardId.PotOfProsperity: return card.Id;
                    case CardId.VWGate_Qinglong: if (!pathUsesQing) return card.Id; break;
                    case CardId.VWGate_Chuche: return card.Id;
                    case CardId.VWGate_Xuanwu: return card.Id;
                }
            }
            return 0;
        }

        private int dumpFromHand(int notCard) 
        {
            // try not to dump settable cards or the notCard
            // unless multiple in hand
            // int setFodder = 0;
            // int multipleNotCard = false;
            // int dump = 0;

            if (Bot.HasInHand(CardId.PSYFrameDriver)) {
                return CardId.PSYFrameDriver;
            }

            if (Bot.HasInHand(CardId.PSYFramegearGamma) && Bot.GetMonsterCount() != 0) {
                return CardId.PSYFramegearGamma;
            }

            if (Bot.Hand.Count(card => card.IsCode(notCard)) > 1) {
                return notCard;
            }

            if (Bot.HasInHand(CardId.Nibiru)) {
                return CardId.Nibiru;
            }

            if (Bot.HasInHand(CardId.VWGate_Xuanwu)) {
                return CardId.VWGate_Xuanwu;
            } else if (Bot.HasInHand(CardId.VWGate_Qinglong)) {
                return CardId.VWGate_Qinglong;
            } else if (Bot.HasInHand(CardId.VW_Toutou)) {
                return CardId.VW_Toutou;
            } else if (Bot.HasInHand(CardId.VW_NiangNiang)) {
                return CardId.VW_NiangNiang;
            } else if (
                Bot.HasInHand(CardId.VWGate_Chuche) && (
                Bot.HasInSpellZone(CardId.VWGate_Chuche) ||
                Bot.Hand.Count(card => card.IsCode(CardId.VWGate_Chuche)) > 1
            )) {
                return CardId.VWGate_Chuche;
            } else if (Bot.HasInHand(CardId.PotOfProsperity)) {
                return CardId.PotOfProsperity;
            } else if (Bot.HasInHand(CardId.VWCity_Kauwloon)) {
                return CardId.VWCity_Kauwloon;
            } else if (Bot.HasInHand(CardId.PotOfDesires)) {
                return CardId.PotOfDesires;
            } else if (Bot.HasInHand(CardId.EmergencyTeleport)) {
                return CardId.EmergencyTeleport;
            } else if (Bot.HasInHand(CardId.InfiniteImpermanence)) {
                return CardId.InfiniteImpermanence;
            } else {
                BotClientCard discard = Bot.Hand.Where(card => !card.IsCode(notCard)).FirstOrDefault();

                return discard != null ? discard.Id : 0;
            }
        }

        private void BootComboPaths()
        {
            if (Bot.HasInExtra(CardId.MuddyMudragon)) {
                if (Bot.HasInHand(CardId.VW_Lulu) && (Bot.HasInHand(CardId.VWCity_Kauwloon) || Bot.HasInSpellZone(CardId.VWGate_Chuche))) {
                    knownComboPaths["Lulu+City"] = true;
                    return;
                } else if (Bot.HasInHand(CardId.VW_Lulu) && (Bot.HasInHand(CardId.VWGate_Qinglong) || Bot.HasInSpellZone(CardId.VWGate_Qinglong))) {
                    knownComboPaths["Lulu+Qing"] = true;
                    return;
                } else if ((Bot.HasInHand(CardId.VW_Lulu) || Bot.HasInHand(CardId.EmergencyTeleport)) && Bot.HasInHand(CardId.VW_Laolao)) {
                    knownComboPaths["Lulu+Lao"] = true;
                    return;
                } else if (Bot.HasInHand(CardId.VW_Lili) && (Bot.HasInHand(CardId.VWCity_Kauwloon) || Bot.HasInSpellZone(CardId.VWGate_Chuche))) {
                    knownComboPaths["Lili+City"] = true;
                    return;
                } else if (Bot.HasInHand(CardId.VW_Lili) && (Bot.HasInHand(CardId.VWGate_Qinglong) || Bot.HasInSpellZone(CardId.VWGate_Qinglong))) {
                    knownComboPaths["Lili+Qing"] = true;
                    return;
                } else if (Bot.HasInHand(CardId.VW_Jiji) && (Bot.HasInHand(CardId.VWCity_Kauwloon) || Bot.HasInSpellZone(CardId.VWGate_Chuche))) {
                    knownComboPaths["Jiji+City"] = true;
                    return;
                } else if (Bot.HasInHand(CardId.VW_Laolao) && (Bot.HasInHand(CardId.VWCity_Kauwloon) || Bot.HasInSpellZone(CardId.VWGate_Chuche))) {
                    knownComboPaths["Lao+City"] = true;
                    return;
                } else if (Bot.HasInHand(CardId.VW_Lulu) && (Bot.HasInHand(CardId.VW_NiangNiang) || Bot.HasInHand(CardId.EmergencyTeleport))) {
                    knownComboPaths["Lulu+Niang"] = true;
                    return;
                } else if (Bot.HasInHand(CardId.VW_Lulu) && Bot.HasInHand(CardId.VW_Jiji)) {
                    knownComboPaths["Lulu+Jiji"] = true;
                    return;
                }
                //  else if (Bot.Hand.Count(card => card.Id == CardId.VW_Jiji) > 1 || ((Bot.HasInHand(CardId.VW_NiangNiang) || Bot.HasInHand(CardId.EmergencyTeleport)) && Bot.HasInHand(CardId.VW_Jiji))) {
                //     knownComboPaths["Jiji+Jiji/Niang"] = true;
                // } 
                else if (Bot.HasInHand(CardId.VW_Jiji) && Bot.HasInHand(CardId.VW_Laolao)) {
                    knownComboPaths["Jiji+Lao"] = true;
                    return;
                }
                //  else if (Bot.HasInHand(CardId.VW_Jiji) && Bot.HasInHand(CardId.VW_Lili)) {
                    // knownComboPaths["Jiji+Lili"] = true;
                    // return;
                // }
                //  else if (Bot.HasInHand(CardId.VW_Lili) && (Bot.HasInHand(CardId.VW_Jiji) || Bot.HasInHand(CardId.VW_NiangNiang))) {
                    // knownComboPaths["Lili+Jiji/Niang"] = true;
                    // return;
                // }
                // else if (Bot.HasInHand(CardId.VW_Lulu) && Bot.HasInHand(CardId.VW_Jiji) && Bot.HasInHand(CardId.VW_Lili)) {
                //     knownComboPaths["Lulu+Jiji+Lili"] = true;
                //     return;
                // } else if (Bot.HasInHand(CardId.VW_Lulu) && Bot.HasInHand(CardId.VW_Jiji) && Bot.HasInHand(CardId.VW_NiangNiang)) {
                //     knownComboPaths["Lulu+Jiji+Niang"] = true;
                //     return;
                // } else if (Bot.HasInHand(CardId.VW_Laolao) && Bot.HasInHand(CardId.VW_Jiji) && Bot.HasInHand(CardId.VW_NiangNiang)) {
                //     knownComboPaths["Lao+Jiji+Niang"] = true;
                //     return;         
                // }
            } else if (Bot.HasInExtra(CardId.VW_Shenshen)) {
                if (Bot.Hand.Count(card => (card.Id == CardId.VW_Lulu)) >= 2 || (Bot.HasInHand(CardId.VW_Lulu) && Bot.HasInHand(CardId.EmergencyTeleport))) {
                    knownComboPaths["Lulu+Lulu"] = true;
                    return;
                }
            } else if (Bot.HasInExtra(CardId.UltimayaTzolkin) && Bot.HasInExtra(CardId.VW_Shenshen)) {
                if ((Bot.HasInHand(CardId.VW_Lulu) || Bot.HasInHand(CardId.EmergencyTeleport)) && Bot.HasInHand(CardId.VW_Lili)) {
                    knownComboPaths["Lulu+Lili"] = true;
                    return;
                } else if ((Bot.HasInHand(CardId.VW_NiangNiang) || Bot.HasInHand(CardId.EmergencyTeleport)) && Bot.HasInHand(CardId.VW_Laolao)) {
                    knownComboPaths["Niang+Lao"] = true;
                    return;
                }
            }


            knownComboPaths["other"] = true;
        }
    }
}
