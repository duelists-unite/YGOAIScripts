using Enumerator;
using System;

namespace DuelBot.Game.AI.Decks
{

    public class DrytronExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Drytron", DuelRules.MasterDuel, (ai, duel) => new DrytronExecutor(ai, duel),
                "s+S3W1LI2Nc8i0UpwZxBVuUtq936Ocwg7HZ1IWPrtU5WGK6JPwXHr6TCGO9LTmHu99Nk+WO7kxWGgwRSmea+L2EG4aMrb7EcOl7ECMNPytnBWLRrJdMVdRmGk8dCGfu/s7DC8Ayp4wwwzPPrMCsMH/J3ZPiRtYRxumIKa7mBL4v1K0EGEM6+cobFdMs+ML6l/Jp5ySxthv18nkxbbjcwyWx7yvQ5v4qpQ7CMGYSNFtUywLDaBCdmEP7bOYcVhp333WWAYfHfs5hgeMufOEZQ+AAA");
        }
        public class CardId
        {
            // Monster Cards
            public const int DivinerOfTheHerald = 92919429;
            public const int DrytronAlphaThuban = 97148796;
            public const int DrytronDeltaAltais = 22420202;
            public const int DrytronGammaEltanin = 60037599;
            public const int DrytronZetaAldhibah = 96026108;
            public const int Eva = 40177746;
            public const int HeraldOfOrangeLight = 17266660;
            public const int CyberAngelBenten = 77235086;
            public const int CyberAngelIdaten = 03629090;
            public const int CyberAngelNatasha = 99427357;
            public const int DrytronMeteonisDraconids = 69815951;
            public const int HeraldOfProfection = 44665365;

            // Spell Cards
            public const int CalledByTheGrave = 24224830;
            public const int CyberEnergency = 60600126;
            public const int DawnOfTheHerald = 27383110;
            public const int ExtraFoolishBurial = 57995165;
            public const int FoolishBurial = 81439173;
            public const int ForbiddenDroplet = 24299458;
            public const int InstantFusion = 01845204;
            public const int MeteonisDrytron = 22398665;
            public const int PotOfProsperity = 84211599;
            public const int PrePreparationOfRites = 13048472;
            public const int PreparationOfRites = 96729612;

            // Trap Cards

            // Extra Cards
            public const int BowOfTheGoddess = 04280258;
            public const int LadyOfTheEternal = 27552504;
            public const int DivineArsenalSkyThunder = 90448279;
            public const int DownerdMagician = 72167543;
            public const int DrytronMuBetaFafnir = 01174075;
            public const int ElderEntityNtss = 80532587;
            public const int HeraldOfTheArcLight = 79606837;
            public const int IPMasquerena = 65741789;
            public const int KnightmarePhoenix = 02857636;
            public const int KnightmareUnicorn = 38342335;
            public const int Linkuriboh = 41999284;
            public const int AssembledNightingale = 48608796;
            public const int MillenniumEyesRestrict = 41578483;

            // Side Cards
            public const int AllyOfJusticeCycleReader = 08233522;
            public const int DarkRulerNoMore = 54693926;
            public const int DrollLockBird = 94145021;
            public const int LightingStorm = 14532163;
            public const int Prohibition = 43711255;
            public const int RedReboot = 23002292;
            
        }

        public DrytronExecutor(GameAI ai, BotDuel duel) : base(ai, duel)
        {
            // Monster Cards
            //AddExecutor(ExecutorType.Activate, CardId., Default);

            // Spell Cards

            // Trap Cards

            // Extra Cards

            // Side Cards

            // Common
            AddExecutor(ExecutorType.MonsterSet);
            AddExecutor(ExecutorType.SpSummon);
            AddExecutor(ExecutorType.Repos, DefaultMonsterRepos);
            AddExecutor(ExecutorType.SpellSet);

        }

        private bool InHand(int id) { return Bot.HasInHand(id); }
        private bool InGraveyard(int id) { return Bot.HasInGraveyard(id); }
        private bool InMonsterZone(int id) { return Bot.HasInMonstersZone(id); }
        private bool IsBanished(int id) { return Bot.HasInBanished(id); }

        public override bool OnSelectHand()
        {
            // go first
            return true;
        }

        private bool Default()
        {
            throw new NotImplementedException();
        }

    }
}
