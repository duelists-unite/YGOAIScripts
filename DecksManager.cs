﻿using System;
using System.Collections.Generic;
using System.Linq;
using Card;
using DuelBot.Game.AI.Decks;
using DuelBot.Game.AI.Decks.Rush;
using Enumerator;

namespace DuelBot.Game.AI
{
    public static class DecksManager
    {
        private static readonly Random _rand = new();
        public static readonly Dictionary<DuelRules, Dictionary<string, Tuple<Func<GameAI, BotDuel, Executor>, Deck>>> DeckTypes = new();
        /// <summary>
        ///  Called once at startup to initialize all deck types
        /// </summary>
        public static void InitAllDecks()
        {
            AI_Custom.Init();
            TestAiExecutor.Init();
            AdamatiaExecutor.Init();            
            AltergeistExecutor.Init();
            BlackwingExecutor.Init();
            BlueEyesExecutor.Init();
            BlueEyesMaxDragonExecutor.Init();
            BurnExecutor.Init();
            ChainBurnExecutor.Init();
            CyberDragonExecutor.Init();
            DarkMagicianExecutor.Init();
            DragunExecutor.Init();
            DragunityExecutor.Init();
            EvilswarmExecutor.Init();
            FrogExecutor.Init();
            FurHireExecutor.Init();
            GravekeeperExecutor.Init();
            //GraydleExecutor.Init();
            GrenMajuThunderBoarderExecutor.Init();
            HorusExecutor.Init();
            IgnisterExecutor.Init();
            Level8Executor.Init();
            LightswornExecutor.Init();
            LightswornShaddoldinosour.Init();
            ManualTestExecutor.Init();
            MathmechExecutor.Init();
            Decks.MokeyMokeyExecutor.Init();
            Decks.MokeyMokeyKingExecutor.Init();
            NekrozExecutor.Init();
            OldSchoolExecutor.Init();
            OrcustExecutor.Init();
            PhantasmExecutor.Init();
            PureWindsExecutor.Init();
            QliphortExecutor.Init();
            RainbowExecutor.Init();
            Rank5Executor.Init();
            SalamangreatExecutor.Init();
            SkyStrikerExecutor.Init();
            ST1732Executor.Init();
            TimeThiefExecutor.Init();
            ToadallyAwesomeExecutor.Init();
            TrickstarExecutor.Init();
            VirtualWorldExecutor.Init();
            YosenjuExecutor.Init();
            ZexalWeaponsExecutor.Init();
            ZoodiacExecutor.Init();
            ResonatorExecutor.Init();
            RushExecutor.Init();
            DinocarriageDynarmixExecutor.Init();
            Decks.Rush.MokeyMokeyExecutor.Init();
            Decks.Rush.MokeyMokeyKingExecutor.Init();            
        }
        public static short TestIndex;
        public static readonly Dictionary<DuelRules, List<short>> CustomIndexes = new();
        /// <summary>
        /// Registers an AI deck to be useable in-game
        /// </summary>
        /// <param name="name">Name of the deck</param>
        /// <param name="rule">Duel rule in which the deck is useable</param>
        /// <param name="creator">Function used to instantiate this AI</param>
        /// <param name="deckHash">Hash of the deck used by the AI</param>
        public static void AddDeckType(string name, DuelRules rule, Func<GameAI, BotDuel, Executor> creator, string deckHash, bool test = false, bool custom = false)
        {
            if (!DeckTypes.ContainsKey(rule))
                DeckTypes.Add(rule, new());
            if (test)
                TestIndex = (short)DeckTypes[rule].Count;
            if (custom)
            {
                if (!CustomIndexes.TryGetValue(rule, out var list))
                {
                    list = new();
                    CustomIndexes[rule] = list;
                }
                list.Add((short)DeckTypes[rule].Count);
            }
            DeckTypes[rule].Add(name, new (creator, new Deck(deckHash) { Name = name }));
            if (!Data.RoomData.BotDecks.ContainsKey(rule))
                Data.RoomData.BotDecks.Add(rule, new());
            Data.RoomData.BotDecks[rule].Add(name);
        }
        public static Executor Instantiate(GameAI ai, BotDuel duel)
        {
            var decks = DeckTypes[duel.Settings.DuelRule == DuelRules.Puzzle ? DuelRules.MasterDuel : duel.Settings.DuelRule];
            short deck = ai.Game.DeckIndex;
            if (deck < 0 || deck >= decks.Count)
            {
                deck = (short)_rand.Next(duel.Settings.DuelRule == DuelRules.Manual ? 0 : 1, decks.Count);
            }
            var t = decks.ElementAt(deck).Value;
            Executor executor = t.Item1(ai, duel);
            executor.Deck = t.Item2;
            return executor;
        }
    }
}
