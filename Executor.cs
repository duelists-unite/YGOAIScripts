﻿using Enumerator;
using System;
using System.Collections.Generic;
using System.Linq;
using Card;
namespace DuelBot.Game.AI
{
    public abstract class Executor
    {
        public Deck Deck { get; set; }
        public BotDuel BotDuel { get; private set; }
        public IList<CardExecutor> Executors { get; private set; }
        protected void ResetExecutors()
        {
            Executors.Clear();
        }
        public GameAI AI { get; private set; }
        public AIUtil Util { get; private set; }

        protected BotMainPhase Main { get; private set; }
        protected BotBattlePhase Battle { get; private set; }

        protected ExecutorType Type { get; private set; }
        protected BotClientCard DynamicCard { get; private set; }
        protected int ActivateDescription { get; private set; }

        protected BotClientField Bot { get; private set; }
        protected BotClientField Enemy { get; private set; }

        protected Executor(GameAI ai, BotDuel duel)
        {
            BotDuel = duel;
            AI = ai;
            Util = new AIUtil(duel);
            Executors = new List<CardExecutor>();

            Bot = BotDuel.Fields[0];
            Enemy = BotDuel.Fields[1];
        }

        public virtual byte OnRockPaperScissors()
        {
            return (byte)BotCore.Rand.Next(1, 4);
        }

        public virtual bool OnSelectHand()
        {
            return BotCore.Rand.Next(2) > 0;
        }

        /// <summary>
        /// Called when the AI has to decide if it should attack
        /// </summary>
        /// <param name="attackers">List of monsters that can attcack.</param>
        /// <param name="defenders">List of monsters of enemy.</param>
        /// <returns>A new BattlePhaseAction containing the action to do.</returns>
        public virtual BattlePhaseAction OnBattle(IList<BotClientCard> attackers, IList<BotClientCard> defenders)
        {
            // For overriding
            return null;
        }

        /// <summary>
        /// Called when the AI has to decide which card to attack first
        /// </summary>
        /// <param name="attackers">List of monsters that can attcack.</param>
        /// <param name="defenders">List of monsters of enemy.</param>
        /// <returns>The card to attack first.</returns>
        public virtual BotClientCard OnSelectAttacker(IList<BotClientCard> attackers, IList<BotClientCard> defenders)
        {
            // For overriding
            return null;
        }

        public virtual BattlePhaseAction OnSelectAttackTarget(BotClientCard attacker, IList<BotClientCard> defenders)
        {
            // Overrided in DefalultExecutor
            return null;
        }

        public virtual bool OnPreBattleBetween(BotClientCard attacker, BotClientCard defender)
        {
            // Overrided in DefalultExecutor
            return true;
        }

        public virtual void OnChaining(int player, BotClientCard card)
        {
            // For overriding
        }

        public virtual void OnChainEnd()
        {
            // For overriding
        }
        public virtual void OnNewPhase()
        {
            // Some AI need do something on new phase
        }
        public virtual void OnNewTurn()
        {
            // Some AI need do something on new turn
        }

        public virtual void OnDraw(int player)
        {
            // Some AI need do something on draw
        }

        public virtual IList<BotClientCard> OnSelectCard(IList<BotClientCard> cards, int min, int max, int hint, bool cancelable)
        {
            // For overriding
            return null;
        }

        public virtual IList<BotClientCard> OnSelectSum(IList<BotClientCard> cards, int sum, int min, int max, int hint, bool mode)
        {
            // For overriding
            return null;
        }

        public virtual IList<BotClientCard> OnSelectFusionMaterial(IList<BotClientCard> cards, int min, int max)
        {
            // For overriding
            return null;
        }

        public virtual IList<BotClientCard> OnSelectSynchroMaterial(IList<BotClientCard> cards, int sum, int min, int max)
        {
            // For overriding
            return null;
        }

        public virtual IList<BotClientCard> OnSelectXyzMaterial(IList<BotClientCard> cards, int min, int max)
        {
            // For overriding
            return null;
        }

        public virtual IList<BotClientCard> OnSelectLinkMaterial(IList<BotClientCard> cards, int min, int max)
        {
            // For overriding
            return null;
        }

        public virtual IList<BotClientCard> OnSelectRitualTribute(IList<BotClientCard> cards, int sum, int min, int max)
        {
            // For overriding
            return null;
        }

        public virtual IList<BotClientCard> OnSelectPendulumSummon(IList<BotClientCard> cards, int max)
        {
            // For overriding
            return null;
        }

        public virtual IList<BotClientCard> OnCardSorting(IList<BotClientCard> cards)
        {
            // For overriding
            return null;
        }

        public virtual bool OnSelectYesNo(int desc)
        {
            return true;
        }

        public virtual int OnSelectOption(IList<int> options)
        {
            return -1;
        }

        public virtual int OnSelectPlace(int cardId, int player, CardLocation location, int available)
        {
            // For overriding
            return 0;
        }

        public virtual CardPosition OnSelectPosition(int cardId, IList<CardPosition> positions)
        {
            // Overrided in DefalultExecutor
            return 0;
        }

        public virtual bool OnSelectBattleReplay()
        {
            // Overrided in DefalultExecutor
            return false;
        }

        public void SetMain(BotMainPhase main)
        {
            Main = main;
        }

        public void SetBattle(BotBattlePhase battle)
        {
            Battle = battle;
        }

        /// <summary>
        /// Set global variables Type, Card, ActivateDescription for Executor
        /// </summary>
        public void SetCard(ExecutorType type, BotClientCard card, int description)
        {
            Type = type;
            DynamicCard = card;
            ActivateDescription = description;
        }

        /// <summary>
        /// Do the action for the card if func return true.
        /// </summary>
        public void AddExecutor(ExecutorType type, int cardId, Func<bool> func)
        {
            Executors.Add(new CardExecutor(type, cardId, func));
        }

        /// <summary>
        /// Do the action for the card if available.
        /// </summary>
        public void AddExecutor(ExecutorType type, int cardId)
        {
            Executors.Add(new CardExecutor(type, cardId, null));
        }

        /// <summary>
        /// Do the action for every card if func return true.
        /// </summary>
        public void AddExecutor(ExecutorType type, Func<bool> func)
        {
            Executors.Add(new CardExecutor(type, -1, func));
        }

        /// <summary>
        /// Do the action for every card if no other Executor is added to it.
        /// </summary>
        public void AddExecutor(ExecutorType type)
        {
            Executors.Add(new CardExecutor(type, -1, DefaultNoExecutor));
        }

        private bool DefaultNoExecutor()
        {
            return Executors.All(exec => exec.Type != Type || exec.CardId != DynamicCard.Id);
        }
    }
}