﻿using Enumerator;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DuelBot.Game.AI
{
    public static class CardContainer
    {
        public static int CompareCardAttack(BotClientCard cardA, BotClientCard cardB)
        {
            if (cardA.Attack < cardB.Attack)
            {
                return -1;
            }

            if (cardA.Attack == cardB.Attack)
            {
                return 0;
            }

            return 1;
        }

        public static int CompareCardLevel(BotClientCard cardA, BotClientCard cardB)
        {
            if (cardA.Level < cardB.Level)
            {
                return -1;
            }

            if (cardA.Level == cardB.Level)
            {
                return 0;
            }

            return 1;
        }

        public static int CompareDefensePower(BotClientCard cardA, BotClientCard cardB)
        {
            if (cardA == null && cardB == null)
            {
                return 0;
            }

            if (cardA == null)
            {
                return -1;
            }

            if (cardB == null)
            {
                return 1;
            }

            int powerA = cardA.GetDefensePower();
            int powerB = cardB.GetDefensePower();
            if (powerA < powerB)
            {
                return -1;
            }

            if (powerA == powerB)
            {
                return 0;
            }

            return 1;
        }

        public static BotClientCard GetHighestAttackMonster(this IEnumerable<BotClientCard> cards, bool canBeTarget = false)
        {
            return cards
                .Where(card => card?.Data != null && card.HasType(CardType.Monster) && card.IsFaceup() && !(canBeTarget && card.IsShouldNotBeTarget()))
                .OrderBy(card => card.Attack).FirstOrDefault();
        }

        public static BotClientCard GetHighestDefenseMonster(this IEnumerable<BotClientCard> cards, bool canBeTarget = false)
        {
            return cards
                .Where(card => card?.Data != null && card.HasType(CardType.Monster) && card.IsFaceup() && !(canBeTarget && card.IsShouldNotBeTarget()))
                .OrderBy(card => card.Defense).FirstOrDefault();
        }

        public static BotClientCard GetLowestAttackMonster(this IEnumerable<BotClientCard> cards, bool canBeTarget = false)
        {
            return cards
                .Where(card => card?.Data != null && card.HasType(CardType.Monster) && card.IsFaceup() && !(canBeTarget && card.IsShouldNotBeTarget()))
                .OrderByDescending(card => card.Attack).FirstOrDefault();
        }

        public static BotClientCard GetLowestDefenseMonster(this IEnumerable<BotClientCard> cards, bool canBeTarget = false)
        {
            return cards
                .Where(card => card?.Data != null && card.HasType(CardType.Monster) && card.IsFaceup() && !(canBeTarget && card.IsShouldNotBeTarget()))
                .OrderByDescending(card => card.Defense).FirstOrDefault();
        }

        public static bool ContainsMonsterWithLevel(this IEnumerable<BotClientCard> cards, int level)
        {
            return cards.Where(card => card?.Data != null).Any(card => !card.HasType(CardType.Xyz) && card.Level == level);
        }

        public static bool ContainsMonsterWithRank(this IEnumerable<BotClientCard> cards, int rank)
        {
            return cards.Where(card => card?.Data != null).Any(card => card.HasType(CardType.Xyz) && card.Rank == rank);
        }

        public static bool ContainsCardWithId(this IEnumerable<BotClientCard> cards, int id)
        {
            return cards.Where(card => card?.Data != null).Any(card => card.IsCode(id));
        }

        public static int GetCardCount(this IEnumerable<BotClientCard> cards, int id)
        {
            return cards.Where(card => card?.Data != null).Count(card => card.IsCode(id));
        }

        public static List<BotClientCard> GetMonsters(this IEnumerable<BotClientCard> cards)
        {
            return cards.Where(card => card?.Data != null && card.HasType(CardType.Monster)).ToList();
        }

        public static List<BotClientCard> GetFaceupPendulumMonsters(this IEnumerable<BotClientCard> cards)
        {
            return cards.Where(card => card?.Data != null && card.HasType(CardType.Monster) && card.IsFaceup() && card.HasType(CardType.Pendulum)).ToList();
        }

        public static BotClientCard GetInvincibleMonster(this IEnumerable<BotClientCard> cards, bool canBeTarget = false)
        {
            return cards.FirstOrDefault(card => card?.Data != null && card.IsMonsterInvincible() && card.IsFaceup() && (!canBeTarget || !card.IsShouldNotBeTarget()));
        }

        public static BotClientCard GetDangerousMonster(this IEnumerable<BotClientCard> cards, bool canBeTarget = false)
        {
            return cards.FirstOrDefault(card => card?.Data != null && card.IsMonsterDangerous() && card.IsFaceup() && (!canBeTarget || !card.IsShouldNotBeTarget()));
        }

        public static BotClientCard GetFloodgate(this IEnumerable<BotClientCard> cards, bool canBeTarget = false)
        {
            return cards.FirstOrDefault(card => card?.Data != null && card.IsFloodgate() && card.IsFaceup() && (!canBeTarget || !card.IsShouldNotBeTarget()));
        }

        public static BotClientCard GetFirstMatchingCard(this IEnumerable<BotClientCard> cards, Func<BotClientCard, bool> filter)
        {
            return cards.FirstOrDefault(card => card?.Data != null && filter.Invoke(card));
        }

        public static BotClientCard GetFirstMatchingFaceupCard(this IEnumerable<BotClientCard> cards, Func<BotClientCard, bool> filter)
        {
            return cards.FirstOrDefault(card => card?.Data != null && card.IsFaceup() && filter.Invoke(card));
        }

        public static IList<BotClientCard> GetMatchingCards(this IEnumerable<BotClientCard> cards, Func<BotClientCard, bool> filter)
        {
            return cards.Where(card => card?.Data != null && filter.Invoke(card)).ToList();
        }

        public static int GetMatchingCardsCount(this IEnumerable<BotClientCard> cards, Func<BotClientCard, bool> filter)
        {
            return cards.Count(card => card?.Data != null && filter.Invoke(card));
        }

        public static bool IsExistingMatchingCard(this IEnumerable<BotClientCard> cards, Func<BotClientCard, bool> filter, int count = 1)
        {
            return cards.GetMatchingCardsCount(filter) >= count;
        }

        public static BotClientCard GetShouldBeDisabledBeforeItUseEffectMonster(this IEnumerable<BotClientCard> cards, bool canBeTarget = true)
        {
            return cards.FirstOrDefault(card => card?.Data != null && card.IsMonsterShouldBeDisabledBeforeItUseEffect() && card.IsFaceup() && (!canBeTarget || !card.IsShouldNotBeTarget()));
        }

        public static IEnumerable<IEnumerable<T>> GetCombinations<T>(this IEnumerable<T> elements, int k)
        {
            return k == 0 ? new[] { new T[0] } :
              elements.SelectMany((e, i) =>
                elements.Skip(i + 1).GetCombinations(k - 1).Select(c => (new[] { e }).Concat(c)));
        }
    }
}