﻿using Enumerator;
using System.Collections.Generic;

namespace DuelBot.Game.AI
{
    public class CardSelector
    {
        private enum SelectType
        {
            Card,
            Cards,
            Id,
            Ids,
            Location
        }

        private readonly SelectType _type;
        private readonly BotClientCard _card;
        private readonly IList<BotClientCard> _cards;
        private readonly int _id;
        private readonly IList<int> _ids;
        private readonly CardLocation _location;

        public CardSelector(BotClientCard card)
        {
            _type = SelectType.Card;
            _card = card;
        }

        public CardSelector(IList<BotClientCard> cards)
        {
            _type = SelectType.Cards;
            _cards = cards;
        }

        public CardSelector(int cardId)
        {
            _type = SelectType.Id;
            _id = cardId;
        }

        public CardSelector(IList<int> ids)
        {
            _type = SelectType.Ids;
            _ids = ids;
        }

        public CardSelector(CardLocation location)
        {
            _type = SelectType.Location;
            _location = location;
        }

        public IList<BotClientCard> Select(IList<BotClientCard> cards, int min, int max)
        {
            IList<BotClientCard> result = new List<BotClientCard>();

            switch (_type)
            {
                case SelectType.Card:
                    if (cards.Contains(_card))
                    {
                        result.Add(_card);
                    }

                    break;
                case SelectType.Cards:
                    foreach (BotClientCard card in _cards)
                    {
                        if (cards.Contains(card) && !result.Contains(card))
                        {
                            result.Add(card);
                        }
                    }

                    break;
                case SelectType.Id:
                    foreach (BotClientCard card in cards)
                    {
                        if (card.IsCode(_id))
                        {
                            result.Add(card);
                        }
                    }

                    break;
                case SelectType.Ids:
                    foreach (int id in _ids)
                    {
                        foreach (BotClientCard card in cards)
                        {
                            if (card.IsCode(id) && !result.Contains(card))
                            {
                                result.Add(card);
                            }
                        }
                    }

                    break;
                case SelectType.Location:
                    foreach (BotClientCard card in cards)
                    {
                        if (card.Location == _location)
                        {
                            result.Add(card);
                        }
                    }

                    break;
            }

            if (result.Count < min)
            {
                foreach (BotClientCard card in cards)
                {
                    if (!result.Contains(card))
                    {
                        result.Add(card);
                    }

                    if (result.Count >= min)
                    {
                        break;
                    }
                }
            }

            while (result.Count > max)
            {
                result.RemoveAt(result.Count - 1);
            }

            return result;
        }
    }
}