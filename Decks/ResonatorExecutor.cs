// Created by Kayden

using System.Collections.Generic;
using Enumerator;

namespace DuelBot.Game.AI.Decks
{
    public class ResonatorExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Resonator", DuelRules.SpeedDuel, (ai, duel) => new ResonatorExecutor(ai, duel),
                "k2bQuHyR/WOJEjMMF+YIMcHwNZtCMHYQiWeE4RbVOgYQbuO5wXo0pJcZhhf8V2WBYdXJ51l/T/dgvf7YkkXD7iMzDIPMBwA=");
        }

        public class CardId
        {
            // Monsters
            public const int wildwind = 52589809; 
            public const int crimson = 34761841;
            public const int red = 40975574;

            // Spells
            public const int call = 23008320;
            public const int cosmic = 08267140;
            public const int destruction = 59593925;

            // Traps
            public const int floodgate = 69599136;

            // Extra Deck
            public const int risingDragon = 66141736;
            public const int darkEndDragon = 88643579;
            public const int redDragonArchfiend = 70902743;
            public const int novaDragon = 97489701;

            // Skill
            public const int demonsResonance = 131191592;
        }

        List<int> canBeDiscarded = new List<int>
        {
            CardId.cosmic,
            CardId.floodgate,
            CardId.destruction
        };

        List<int> Resonators = new List<int>
        {
            CardId.red,
            CardId.crimson
        };

        public bool wasDemonsResonanceActivated = false;

        public ResonatorExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.Activate, CardId.call, callEff);
            AddExecutor(ExecutorType.Activate, CardId.demonsResonance, demonsEff);
            AddExecutor(ExecutorType.Activate, CardId.destruction, destructionEff);
            AddExecutor(ExecutorType.Activate, CardId.crimson, crimsonSpSummon);
            AddExecutor(ExecutorType.SpSummon, CardId.wildwind, wildwindSpSummon); 
            AddExecutor(ExecutorType.SpSummon, CardId.redDragonArchfiend, dragonArchfiendSummon);

            AddExecutor(ExecutorType.SpSummon, CardId.novaDragon);
            AddExecutor(ExecutorType.Activate, CardId.novaDragon, novaEff);

            AddExecutor(ExecutorType.SpSummon, CardId.risingDragon);
            AddExecutor(ExecutorType.Activate, CardId.risingDragon, risingEff);
            
            AddExecutor(ExecutorType.MonsterSet, CardId.wildwind, setWildwind);
            AddExecutor(ExecutorType.Activate, CardId.wildwind, wildwindEff);
            AddExecutor(ExecutorType.Summon, CardId.red, redSummon);
            AddExecutor(ExecutorType.Activate, CardId.red, redEff);
            AddExecutor(ExecutorType.Summon, CardId.crimson, crimsonSummon);

            AddExecutor(ExecutorType.Activate, CardId.cosmic, DefaultCosmicCyclone);
            AddExecutor(ExecutorType.SpellSet, CardId.cosmic);

            AddExecutor(ExecutorType.SpellSet, CardId.floodgate);
            AddExecutor(ExecutorType.Activate, CardId.floodgate, floodgateEff);

            AddExecutor(ExecutorType.SpSummon, CardId.darkEndDragon);
            AddExecutor(ExecutorType.Activate, CardId.darkEndDragon);
        }

        public override bool OnSelectHand()
        {
            return false;
        }

        public override bool OnPreBattleBetween(BotClientCard attacker, BotClientCard defender)
        {
            if (defender.IsAttack() && attacker.Attack > defender.Attack)
            {
                return true;
            }
            if (defender.IsDefense() && attacker.Attack > defender.Defense)
            {
                return true;
            }
            return false;
        }

        public override void OnNewTurn()
        {
            wasDemonsResonanceActivated = false;
        }

        public bool callEff()
        {
            AI.SelectCard(CardId.crimson);
            return true;
        }
        public bool crimsonSpSummon()
        {
            if (Bot.HasInHand(CardId.crimson) && Bot.IsFieldEmpty() && Bot.HasInHand(CardId.wildwind) && Bot.GetMonsterCount() < 3)
            {
                return true;
            }
            else if (Bot.HasInMonstersZone(CardId.crimson) && Bot.GetMonsterCount() < 3)
            {
                return true;
            }
            return false;
        }
        public bool wildwindSpSummon()
        {
            if (!Bot.HasInMonstersZone(CardId.wildwind) && Bot.GetMonsterCount() < 3)
            {
                return true;
            }
            return false;
        }
        public bool setWildwind()
        {
            if (Bot.IsFieldEmpty() && !Bot.HasInHand(CardId.crimson) && !Bot.HasInHand(CardId.red))
            {
                return true;
            }
            return false;
        }
        public bool wildwindEff()
        {
            AI.SelectCard(CardId.crimson);
            return true;
        }
        public bool redSummon()
        {
            if (!Bot.HasInMonstersZone(CardId.crimson) && Bot.HasInHand(CardId.wildwind) && Bot.GetMonsterCount() < 2)
            {
                return true;
            }
            return false;
        }
        public bool redEff()
        {
            int highestATKMonster = 0;
            List<BotClientCard> all_monsters = Enemy.GetMonsters();
            List<BotClientCard> bot_monsters = Bot.GetMonsters();
            all_monsters.AddRange(bot_monsters);
            foreach (BotClientCard card in all_monsters)
            {
                if (card.Attack > highestATKMonster)
                {
                    highestATKMonster = card.Attack;
                }
            }
            foreach (BotClientCard card in all_monsters)
            {
                if (card.Attack == highestATKMonster)
                {
                    AI.SelectCard(card);
                    return true;
                }
            }
            return false;
        }
        public bool destructionEff()
        {
            if (!Enemy.IsFieldEmpty() && !Bot.HasInSpellZone(CardId.destruction) && Bot.HasInMonstersZone(CardId.wildwind) && Bot.HasInMonstersZone(Resonators))
            {
                return true;
            }
            return false;
        }
        public bool floodgateEff()
        {
            BotClientCard LastChain = Util.GetLastChainCard();
            if (LastChain != null && LastChain.Id == CardId.floodgate)
            {
                return false;
            }
            return true;
        }
        public bool risingEff()
        {
            AI.SelectCard(CardId.crimson);
            return true;
        }
        public bool crimsonSummon()
        {
            if (Bot.HasInMonstersZone(CardId.redDragonArchfiend) && Bot.GetMonsterCount() < 3 && Bot.HasInMonstersZone(Resonators))
            {
                return true;
            }
            if (Bot.HasInMonstersZone(CardId.novaDragon) && Bot.HasInHand(CardId.wildwind))
            {
                return true;
            }
            return false;
        }
        public bool dragonArchfiendSummon()
        {
            if (BotDuel.Turn == 1)
            {
                return false;
            }
            return true;
        }
        public bool novaEff()
        {
            BotClientCard nova = Bot.GetMonsters().GetHighestAttackMonster();
            if (Enemy.BattlingMonster.Attack > nova.Attack)
            {
                return true;
            }
            return false;
        }
        public bool demonsEff()
        {
            int numOfResonators = 0;
            if (!Bot.HasInHand(CardId.wildwind) && wasDemonsResonanceActivated == false)
            {
                AI.SelectCard(CardId.red, CardId.crimson);
                foreach (BotClientCard card in Bot.Hand)
                {
                    if (card.Id == CardId.red || card.Id == CardId.crimson)
                    {
                        numOfResonators += 1;
                    }
                }
                if (numOfResonators > 2)
                {
                    AI.SelectNextCard(CardId.red, CardId.crimson);
                } 
                else
                {
                    AI.SelectNextCard(canBeDiscarded);
                }
                
                AI.SelectThirdCard(CardId.wildwind);
                wasDemonsResonanceActivated = true;
                return true;
            }
            return false;
        }
    }
}
