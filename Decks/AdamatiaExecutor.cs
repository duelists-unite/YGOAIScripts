using System.Collections.Generic;
using Enumerator;
namespace DuelBot.Game.AI.Decks
{
    public class AdamatiaExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Adamatia", DuelRules.MasterDuel, (ai, duel) => new AdamatiaExecutor(ai, duel), 
                "M2TYdn0Ja4D7eiYY5i9fwwDDxz/PgWOF27eYYLjiQBkLDK9XvscAw7ujnjDBcDRvMxwf+izHCsP2vFJMIJy/z5DF1jYSjiOrk1maGeRY7ZYUMsKwT8gxpsqsCQwwXFR6nQmGZWSiWWAY5AcA");
        }
        public class CardId
        {
            public const int AdamatiaRiseDragite = 09464441;
            public const int AdamatiaRiseLeonite = 47674738;
            public const int AdamatiaRiseRaptite = 73079836;
            public const int AdamatiaAnalyzer = 11302671;
            public const int AdamatiaSeeker = 48519867;
            public const int AdamatiaResearcher = 85914562;
            public const int AdamatiaCrystaDragite = 10286023;
            public const int AdamatiaCrystaLeonite = 47897376;
            public const int AdamatiaCrystaRaptite = 74891384;
            public const int KoakiMeiruGuardian = 45041488;
            public const int DokiDoki = 42143067;
            public const int AshBlossom = 14558127;
            public const int BlockDragon = 94689206;
            // Spells
            public const int UpstartGoblin = 7036887;
            public const int PotOfDesires = 35261759;
            public const int AdamatiaSign = 72957245;
            public const int Terraforming = 73628505;
            public const int MagicalMallet = 85852291;
            public const int CalledByTheGrave = 24224830;
            public const int AdamatiaLaputite = 46552140;
        }

        List<int> Eggs = new List<int>
        {
            CardId.KoakiMeiruGuardian,
            CardId.AdamatiaCrystaDragite,
            CardId.AdamatiaCrystaLeonite,
            CardId.AdamatiaCrystaRaptite,
            CardId.DokiDoki,
            CardId.AdamatiaRiseRaptite
        };

        List<int> Tuners = new List<int>
        {
            CardId.AdamatiaSeeker,
            CardId.AdamatiaAnalyzer,
            CardId.AdamatiaResearcher
        };

        List<int> Set = new List<int>
        {
            CardId.AdamatiaSeeker,
            CardId.AdamatiaAnalyzer,
            CardId.AdamatiaResearcher,
            CardId.DokiDoki
        };

        bool WasAnalyzerActivated = false;
        bool WasSeekerActivated = false;
        bool WasResearcherActivated = false;

        public AdamatiaExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.Activate, CardId.UpstartGoblin);
            AddExecutor(ExecutorType.Activate, CardId.PotOfDesires);
            AddExecutor(ExecutorType.Activate, CardId.Terraforming);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaLaputite, when_laputite);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaLaputite, change_order);
            AddExecutor(ExecutorType.Activate, CardId.CalledByTheGrave, DefaultCalledByTheGrave);
            AddExecutor(ExecutorType.Activate, CardId.AshBlossom, DefaultAshBlossomAndJoyousSpring);
            AddExecutor(ExecutorType.Activate, CardId.MagicalMallet, when_mallet);

            AddExecutor(ExecutorType.Summon, CardId.DokiDoki, when_doki_summon);
            AddExecutor(ExecutorType.Activate, CardId.DokiDoki, when_doki_activate);

            AddExecutor(ExecutorType.Activate, CardId.AdamatiaAnalyzer, when_analyzer_sp);
            AddExecutor(ExecutorType.Summon, CardId.AdamatiaAnalyzer, when_analyzer_summon);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaAnalyzer, when_analyzer_excavate);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaResearcher, when_researcher_sp);
            AddExecutor(ExecutorType.Summon, CardId.AdamatiaResearcher, when_researcher_summon);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaResearcher, when_researcher_excavate);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaSeeker, when_seeker_sp);
            AddExecutor(ExecutorType.Summon, CardId.AdamatiaSeeker, when_seeker_summon);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaSeeker, when_seeker_excavate);

            AddExecutor(ExecutorType.SpSummon, CardId.AdamatiaRiseRaptite, when_raptite_sp);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaRiseRaptite, when_raptite_excavate);
            AddExecutor(ExecutorType.SpSummon, CardId.AdamatiaRiseLeonite, when_leonite_sp);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaRiseLeonite, when_leonite_excavate);
            AddExecutor(ExecutorType.SpSummon, CardId.AdamatiaRiseDragite, when_dragite_sp);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaRiseDragite, when_dragite_excavate);

            AddExecutor(ExecutorType.Activate, CardId.AdamatiaCrystaDragite, top_of_deck_dragite);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaCrystaRaptite, top_of_deck_raptite);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaCrystaLeonite, top_of_deck_leonite);
            AddExecutor(ExecutorType.Activate, CardId.AdamatiaCrystaDragite, when_dragite_draw);

            AddExecutor(ExecutorType.MonsterSet, CardId.AdamatiaCrystaDragite, when_set_dragite);
            AddExecutor(ExecutorType.MonsterSet, CardId.AdamatiaCrystaLeonite, when_set_leonite);
            AddExecutor(ExecutorType.MonsterSet, CardId.AdamatiaCrystaRaptite, when_set_raptite);

            AddExecutor(ExecutorType.SpSummon, CardId.BlockDragon, when_blockdragon_sp);
            AddExecutor(ExecutorType.Activate, CardId.BlockDragon, when_blockdragon_add);

        }
        public override void OnNewTurn()
        {
            WasAnalyzerActivated = false;
            WasResearcherActivated = false;
            WasSeekerActivated = false;
        }
        public override bool OnSelectHand()
        {
            return true;
        }
        private bool when_laputite()
        {
            if (Bot.HasInHand(CardId.AdamatiaLaputite))
            {
                return true;
            }
            return false;
        }
        private bool when_mallet()
        {
            if (!Bot.HasInHand(CardId.AdamatiaAnalyzer) && !Bot.HasInHand(CardId.AdamatiaResearcher) && !Bot.HasInHand(CardId.AdamatiaSeeker))
            {
                AI.SelectCard(
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaLeonite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.AdamatiaSign,
                    CardId.AshBlossom,
                    CardId.CalledByTheGrave,
                    CardId.DokiDoki,
                    CardId.KoakiMeiruGuardian
                    );
                return true;
            }
            return false;
        }
        private bool when_doki_summon()
        {
            if (!Bot.HasInHand(CardId.AdamatiaAnalyzer) && !Bot.HasInHand(CardId.KoakiMeiruGuardian))
                return false;
            return true;
        }
        private bool when_doki_activate()
        {
            AI.SelectCard(
                CardId.KoakiMeiruGuardian,
                CardId.AdamatiaAnalyzer
                );
            AI.SelectNextCard(
                CardId.AdamatiaAnalyzer
                );
            return true;
        }
        private bool when_analyzer_sp()
        {
            if (Bot.HasInHand(CardId.AdamatiaAnalyzer))
            {
                return true;
            }
            return false;
        }
        private bool when_analyzer_summon()
        {
            return true;
        }
        private bool when_analyzer_excavate()
        {
            if (Bot.GetMonsterCount() > 4)
            {
                return false;
            }
            if (Bot.HasInHand(CardId.KoakiMeiruGuardian) || Bot.HasInHand(CardId.AdamatiaAnalyzer) && Bot.GetMonsterCount() < 5)
            {
                AI.SelectCard(
                    CardId.DokiDoki,
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.AdamatiaCrystaLeonite,
                    CardId.KoakiMeiruGuardian
                    );
                WasAnalyzerActivated = true;
                return true;
            }
            if (Bot.GetMonsterCount() < 5)
            {
                AI.SelectCard(
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaLeonite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.KoakiMeiruGuardian,
                    CardId.DokiDoki
                    );
                WasAnalyzerActivated = true;
                return true;
            }
            return false;
        }
        private bool when_seeker_sp()
        {
            if (Bot.HasInHand(CardId.AdamatiaSeeker))
            {
                return true;
            }
            return false;
        }
        private bool when_seeker_summon()
        {
            return true;
        }
        private bool when_seeker_excavate()
        {
            if (Bot.GetMonsterCount() > 4)
            {
                return false;
            }
            if (Bot.HasInHand(CardId.KoakiMeiruGuardian) || Bot.HasInHand(CardId.AdamatiaAnalyzer) && Bot.GetMonsterCount() < 5)
            {
                AI.SelectCard(
                    CardId.DokiDoki,
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.AdamatiaCrystaLeonite,
                    CardId.KoakiMeiruGuardian
                    );
                WasSeekerActivated = true;
                return true;
            }
            if (Bot.GetMonsterCount() < 5)
            {
                AI.SelectCard(
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.AdamatiaCrystaLeonite,
                    CardId.KoakiMeiruGuardian,
                    CardId.DokiDoki
                    );
                WasSeekerActivated = true;
                return true;
            }
            return false;
        }
        private bool when_researcher_sp()
        {
            if (Bot.HasInHand(CardId.AdamatiaResearcher))
            {
                return true;
            }
            return false;
        }
        private bool when_researcher_summon()
        {
            return true;
        }
        private bool when_researcher_excavate()
        {
            if (Bot.GetMonsterCount() > 4)
            {
                return false;
            }
            if (Bot.HasInHand(CardId.KoakiMeiruGuardian) || Bot.HasInHand(CardId.AdamatiaAnalyzer) && Bot.GetMonsterCount() < 5)
            {
                AI.SelectCard(
                    CardId.DokiDoki,
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.AdamatiaCrystaLeonite,
                    CardId.KoakiMeiruGuardian
                    );
                WasResearcherActivated = true;
                return true;
            }
            if (Bot.GetMonsterCount() < 5)
            {
                AI.SelectCard(
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.AdamatiaCrystaLeonite,
                    CardId.KoakiMeiruGuardian,
                    CardId.DokiDoki
                    );
                WasResearcherActivated = true;
                return true;
            }
            return false;
        }
        private bool when_raptite_sp()
        {
            if (Bot.HasInMonstersZone(CardId.AdamatiaRiseRaptite))
            {
                return false;
            }
            if (Bot.HasInMonstersZone(Eggs) && Bot.HasInMonstersZone(Tuners))
            {
                if (WasSeekerActivated)
                {
                    AI.SelectCard(CardId.AdamatiaSeeker);
                    return true;
                }
                if (WasResearcherActivated)
                {
                    AI.SelectCard(CardId.AdamatiaSeeker);
                    return true;
                }
                if (WasAnalyzerActivated)
                {
                    AI.SelectCard(CardId.AdamatiaAnalyzer);
                    return true;
                }
                return true;
            }
            return false;
        }
        private bool when_leonite_sp()
        {
            if (Bot.HasInMonstersZone(CardId.AdamatiaRiseLeonite))
            {
                return false;
            }
            if (Bot.HasInMonstersZone(Eggs) && Bot.HasInMonstersZone(Tuners))
            {
                if (WasSeekerActivated)
                {
                    AI.SelectCard(CardId.AdamatiaSeeker);
                    return true;
                }
                if (WasResearcherActivated)
                {
                    AI.SelectCard(CardId.AdamatiaResearcher);
                    return true;
                }
                if (WasAnalyzerActivated)
                {
                    AI.SelectCard(CardId.AdamatiaAnalyzer);
                    return true;
                }
                return true;
            }
            return false;
        }


        private bool when_leonite_excavate()
        {
            if (!WasAnalyzerActivated)
            {
                AI.SelectCard(
                    CardId.AdamatiaAnalyzer,
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.AdamatiaCrystaLeonite,
                    CardId.KoakiMeiruGuardian
                    );
                return true;
            }
            else if (!WasSeekerActivated)
            {
                AI.SelectCard(
                    CardId.AdamatiaSeeker,
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.AdamatiaCrystaLeonite,
                    CardId.KoakiMeiruGuardian
                    );
                return true;
            }
            else if (!WasResearcherActivated)
            {
                AI.SelectCard(
                    CardId.AdamatiaResearcher,
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.AdamatiaCrystaLeonite,
                    CardId.KoakiMeiruGuardian
                    );
                return true;
            }
            return false;
        }
        private bool when_raptite_excavate()
        {
            if (Bot.GetMonsterCount() > 4)
            {
                return false;
            }
            if (!WasAnalyzerActivated)
            {
                AI.SelectCard(
                    CardId.AdamatiaAnalyzer,
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.AdamatiaCrystaLeonite,
                    CardId.KoakiMeiruGuardian
                    );
                return true;
            }
            else if (!WasSeekerActivated)
            {
                AI.SelectCard(
                    CardId.AdamatiaSeeker,
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.KoakiMeiruGuardian,
                    CardId.AdamatiaCrystaLeonite
                    );
                return true;
            }
            else if (!WasResearcherActivated)
            {
                AI.SelectCard(
                    CardId.AdamatiaResearcher,
                    CardId.AdamatiaCrystaDragite,
                    CardId.AdamatiaCrystaRaptite,
                    CardId.AdamatiaCrystaLeonite,
                    CardId.KoakiMeiruGuardian
                    );
                return true;
            }
            return false;
        }
        private bool when_dragite_sp()
        {
            if (Bot.HasInMonstersZone(CardId.AdamatiaRiseDragite))
            {
                return false;
            }
            if (Bot.HasInMonstersZone(Eggs) && Bot.HasInMonstersZone(Tuners))
            {
                return true;
            }
            return false;
        }
        private bool when_dragite_excavate()
        {
            if (Enemy.GetMonsterCount() < 1)
            {
                return false;
            }
            else
            {
                AI.SelectCard(Util.GetBestEnemyCard());
                return true;
            }
        }
        private bool when_dragite_draw()
        {
            if (Bot.HasInGraveyard(CardId.AdamatiaCrystaDragite))
            {
                return false;
            }
            else
            {
                return true;
            }
            
        }
        private bool when_set_dragite()
        {
            if (!Bot.HasInHand(Set))
            {
                return true;
            }
            return false;
        }
        private bool when_set_leonite()
        {
            if (!Bot.HasInHand(Set))
            {
                return true;
            }
            return false;
        }
        private bool when_set_raptite()
        {
            if (!Bot.HasInHand(Set))
            {
                return true;
            }
            return false;
        }
        private bool top_of_deck_raptite()
        {
            return false;
        }
        private bool top_of_deck_leonite()
        {
            return false;
        }
        private bool top_of_deck_dragite()
        {
            return false;
        }
        private bool change_order()
        {
            if (Bot.HasInSpellZone(CardId.AdamatiaLaputite))
            {
                return true;
            }
            return false;
        }
        private bool when_blockdragon_sp()
        {
            return true;
        }
        private bool when_blockdragon_add()
        {
            AI.SelectCard(CardId.AdamatiaAnalyzer);
            AI.SelectNextCard(CardId.AdamatiaSeeker);
            AI.SelectThirdCard(CardId.AdamatiaResearcher);
            return true;
        }
    }
}