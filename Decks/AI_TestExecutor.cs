using Enumerator;
namespace DuelBot.Game.AI.Decks
{
    public class TestAiExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("AI_Test", DuelRules.MasterDuel, (ai, duel) => new TestAiExecutor(ai, duel),
            "M2eIsH7J6lcgywDDH0RdGGH4VLIpMwwzVd5mgeFVgScZYNiq4g0jDKd9LmeGYakQa1YYbtdkY4DhY8dnwPHz9ChGEK5xZGSFYYkoaSYQ3sC6nxWExS6JM8DwdSUVOD7iosgKw82P7Zhg+JK0IyMMg/wGAA==", true);
        }
        public class CardId
        {
            //Monsters
            public const int Kozaky = 99171160;
            public const int KeyMace = 01929294;
            public const int BoneMouse = 21239280;
            public const int FungiMusk = 53830602;
            public const int AncientJar = 81492226;
            public const int DarkPlant = 13193642;
            public const int SkullServant = 32274490;
            public const int PetitMoth = 58192742;
            public const int Watapon = 87774234;
            //Spells
            public const int AForces = 00403847;
            public const int Courage = 10012614;
            public const int Umi = 22702055;
            //Traps
            public const int SolemnWishes = 35346968;
            public const int JarOfGreed = 83968380;
            public const int Recall = 96404912;
        }

        public TestAiExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            //Monsters
            AddExecutor(ExecutorType.Summon, CardId.Kozaky);
            AddExecutor(ExecutorType.Summon, CardId.KeyMace);
            AddExecutor(ExecutorType.Summon, CardId.BoneMouse);
            AddExecutor(ExecutorType.Summon, CardId.FungiMusk);
            AddExecutor(ExecutorType.Summon, CardId.AncientJar);
            AddExecutor(ExecutorType.Summon, CardId.DarkPlant);
            AddExecutor(ExecutorType.Summon, CardId.SkullServant);
            AddExecutor(ExecutorType.Summon, CardId.PetitMoth);
            AddExecutor(ExecutorType.Summon, CardId.Watapon);
            //Traps
            AddExecutor(ExecutorType.SpellSet, CardId.SolemnWishes);
            AddExecutor(ExecutorType.SpellSet, CardId.Recall);
            AddExecutor(ExecutorType.SpellSet, CardId.JarOfGreed);

            AddExecutor(ExecutorType.Activate, CardId.SolemnWishes, when_solemn);
            AddExecutor(ExecutorType.Activate, CardId.JarOfGreed, when_jar);
            AddExecutor(ExecutorType.Activate, CardId.Recall, when_recall);
            //Spells
            AddExecutor(ExecutorType.Activate, CardId.AForces);
            AddExecutor(ExecutorType.Activate, CardId.Courage);
            AddExecutor(ExecutorType.Activate, CardId.Umi);
        }
        public bool when_solemn()
        {
            return false;
        }
        public bool when_jar()
        {
            return true;
        }
        public bool when_recall()
        {
            return false;
        }
    }
}
