using Enumerator;

namespace DuelBot.Game.AI.Decks
{
    public class MokeyMokeyKingExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Mokey Mokey King", DuelRules.MasterDuel, (ai, duel) => new MokeyMokeyKingExecutor(ai, duel),
                "M2QoTwqG43SmcCYYXmu1gQGGz8/5xArDDffz4DiQfwoLDEcekGKF4YgpWSwwfDpAFI4TkhcwwjDfCXXG5+lRjNF5qYxmS1WYvrTvYnJ8yMEcMSGe2bfyE4uIrCprmLgZa7DEbiYYdit0YYHhWeXscAwA");
        }
        public class CardId
        {

            // Monster Cards
            public const int BlazingInpachi = 05464695;
            public const int BetaMagnetWarrior = 39256679;
            public const int GammaMagnetWarrior = 11549357;
            public const int AlphaMagnetWarrior = 99785935;
            public const int CelticGuardian = 91152256;
            public const int HarpieLady = 76812113;
            public const int AquaMadoor = 85639257;
            public const int TuneWarrior = 74093656;
            public const int GenexController = 68505803;
            public const int MokeyMokey = 27288416;
            public const int LeoWizard = 4392470;
            public const int Bunilla = 69380702;

            // Spell Cards
            public const int MoltenDestruction = 19384334;
            public const int Umi = 22702055;
            public const int Wasteland = 23424603;
            public const int AcidicDownpour = 35956022;
            public const int RisingAirCurrent = 45778932;
            public const int Mountain = 50913601;
            public const int GaiaPower = 56594520;
            public const int Umiiruika = 82999629;
            public const int Sogen = 86318356;
            public const int Forest = 87430998;

            // Extra Cards
            public const int ScrapArchfiend = 45815891;
            public const int GemknightPearl = 71594310;
            public const int LightingShadow = 67598234;

        }

        public MokeyMokeyKingExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.SpSummon, CardId.LeoWizard);
            AddExecutor(ExecutorType.SummonOrSet, CardId.Bunilla);
            AddExecutor(ExecutorType.Repos, DefaultMonsterRepos);
            AddExecutor(ExecutorType.Activate, DefaultField);
        }
    }
}
