using Enumerator;
namespace DuelBot.Game.AI.Decks
{
    // NOT FINISHED YET
    public class EvilswarmExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Evilswarm", DuelRules.MasterDuel, (ai, duel) => new EvilswarmExecutor(ai, duel),
                "M+ffYDqfBYZPZhYwPcs/yALDa+f0MIPwDr1AlqlTPBhgOL+xDY5lZIVZYVhraiojCHO/4mV43yQGxr/uiDAum/2FsUW1jkHYfgkjCF/tnA7GN90jGWYUyzAFCjQzg/DtqdlMMPybpYKJQY+X1dvLi9W2ppfhmHEyc0eSBuvmewZMCnsns16SdmQMuVjA+nDDSSb/h29Ytn5gZN21sYOlu4aVASQOwkaLahlA+L16IAPI/p9GR+G4SkGUEYT3tK5kgGFJ57msIAxyCwA=");
        }
        public class CardId
        {
            public const int DarkHole = 53129443;
            public const int CosmicCyclone = 8267140;
            public const int InfestationPandemic = 27541267;
            public const int SolemnJudgment = 41420027;
            public const int SolemnWarning = 84749824;
            public const int SolemnStrike = 40605147;
        }

        public EvilswarmExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.Activate, CardId.DarkHole, DefaultDarkHole);
            AddExecutor(ExecutorType.Activate, CardId.CosmicCyclone, DefaultCosmicCyclone);
            AddExecutor(ExecutorType.Activate, CardId.SolemnJudgment, DefaultSolemnJudgment);
            AddExecutor(ExecutorType.Activate, CardId.SolemnWarning, DefaultSolemnWarning);
            AddExecutor(ExecutorType.Activate, CardId.SolemnStrike, DefaultSolemnStrike);
            AddExecutor(ExecutorType.SpellSet, CardId.InfestationPandemic);
            AddExecutor(ExecutorType.Activate, DefaultDontChainMyself);
            AddExecutor(ExecutorType.Summon);
            AddExecutor(ExecutorType.SpSummon);
            AddExecutor(ExecutorType.Repos, DefaultMonsterRepos);
            AddExecutor(ExecutorType.SpellSet);
        }

        // will be added soon...?
    }
}
