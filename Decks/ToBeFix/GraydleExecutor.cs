using Enumerator;
using System;

namespace DuelBot.Game.AI.Decks
{
    // NOT FINISHED YET
    public class GraydleExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Graydle", DuelRules.MasterDuel, (ai, duel) => new GraydleExecutor(ai, duel),
                "M+ffwWXECMPHrY/D8aXbE1lhePP3r8wwrDU1lRGGNxmdYYXh33dEGOfn/IDjxxu0mN+p/mN5bPaQFYa3yGgxwHDpjTLmLa9rWUCYOe8XEwwvTHvHAsO/WSqYdL4oM7PdO8dy7HgcE99aaWYQvv7YkqWhZjLLPuXlTI7LGVmvG61i5eq/zrrdy57V5tRURvnH91mvT17BktR+jAmG36sHMsCwg9omVhhenHWFCYbn6T5khWEA");
        }
        public class CardId
        {
            // Monster Cards
            public const int GraydleSlime = 20056760;
            public const int GraydleEagle = 29834183;
            public const int GraydleCobra = 93445074;
            public const int GraydleAlligator = 66451379;
            public const int MaxxC = 23434538;
            public const int EffectVeiler = 97268402;
            public const int CyberDragon = 70095154;

            // Spell Cards
            public const int FeatherDister = 18144507;
            public const int Surface = 33057951;
            public const int DarkHole = 53129443;
            public const int MonsterReborn = 83764718;
            public const int PotOfDuality = 98645731;
            public const int GraydleImpact = 02759860;
            public const int PowerBond = 37630732;

            // Trap Cards
            public const int StarlightRoad = 58120309;
            public const int GraydleSplit = 75361204;
            public const int GraydleParasite = 49966595;
            public const int SkillDrain = 82732705;
            public const int SolemnJudgment = 41420027;

            // Extra Cards
            public const int Trishukla = 52687916;
            public const int ScarlightRedDragon = 80666118;
            public const int HotRedDragon = 39765958;
            public const int GreydleDragon = 52145422;
            public const int RedDragonArchfiend = 70902743;
            public const int ScrapDragon = 76774528;
            public const int StardustDragon = 44508094;
            public const int StardustSparkDragon = 83994433;
            public const int BlackwingNothung = 95040215;
            public const int VulcanTheDivine = 98012938;
            public const int ArmadesKeeper = 88033975;
            public const int AllyOfJustice = 26593852;
            public const int TGWonderMagician = 98558751;
            public const int WindUpZenmaines = 78156759;

            // Side Cards
            public const int SantaClaws = 46565218;
            public const int MysticalSpaceTyphoon = 05318639;
            public const int PoisonousWinds = 95561280;
            public const int DrowningMirrorForce = 47475363;
            public const int DiamondDust = 98643358;

        }

        public GraydleExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            // Monster Cards
            AddExecutor(ExecutorType.Activate, CardId.GraydleSlime, DefaultGraydleSlime);
            AddExecutor(ExecutorType.Activate, CardId.GraydleEagle, DefaultGraydleEagle);
            AddExecutor(ExecutorType.Activate, CardId.GraydleCobra, DefaultGraydleCobra);
            AddExecutor(ExecutorType.Activate, CardId.GraydleAlligator, DefaultGraydleAlligator);
            AddExecutor(ExecutorType.Activate, CardId.MaxxC, DefaultMaxxC);
            AddExecutor(ExecutorType.Activate, CardId.EffectVeiler, DefaultEffectVeiler);

            // Spell Cards
            AddExecutor(ExecutorType.Activate, CardId.PowerBond, PowerBondEffect);
            AddExecutor(ExecutorType.Activate, CardId.FeatherDister, FeatherDisterEffect);
            AddExecutor(ExecutorType.Activate, CardId.Surface, SurfaceEffect);
            AddExecutor(ExecutorType.Activate, CardId.DarkHole, DarkHoleEffect);
            AddExecutor(ExecutorType.Activate, CardId.MonsterReborn, MonsterRebornEffect);
            AddExecutor(ExecutorType.Activate, CardId.PotOfDuality, PotOfDualityEffect);
            AddExecutor(ExecutorType.Activate, CardId.GraydleImpact, GraydleImpactEffect);

            // Trap Cards
            AddExecutor(ExecutorType.Activate, CardId.StarlightRoad, StarlightRoadTrap);
            AddExecutor(ExecutorType.Activate, CardId.GraydleSplit, GraydleSplitTrap);
            AddExecutor(ExecutorType.Activate, CardId.GraydleParasite, GraydleParasiteTrap);
            AddExecutor(ExecutorType.Activate, CardId.SkillDrain, SkillDrainTrap);
            AddExecutor(ExecutorType.Activate, CardId.SolemnJudgment, SolemnJudgmentTrap);

            // Extra Cards
            AddExecutor(ExecutorType.SpSummon, CardId.CyberDragon);
            AddExecutor(ExecutorType.SpSummon, CardId.Trishukla);
            AddExecutor(ExecutorType.SpSummon, CardId.ScarlightRedDragon);
            AddExecutor(ExecutorType.SpSummon, CardId.HotRedDragon);
            AddExecutor(ExecutorType.SpSummon, CardId.GreydleDragon);
            AddExecutor(ExecutorType.SpSummon, CardId.RedDragonArchfiend);
            AddExecutor(ExecutorType.SpSummon, CardId.ScrapDragon);
            AddExecutor(ExecutorType.SpSummon, CardId.StardustDragon);
            AddExecutor(ExecutorType.SpSummon, CardId.StardustSparkDragon);
            AddExecutor(ExecutorType.SpSummon, CardId.BlackwingNothung);
            AddExecutor(ExecutorType.SpSummon, CardId.VulcanTheDivine);
            AddExecutor(ExecutorType.SpSummon, CardId.ArmadesKeeper);
            AddExecutor(ExecutorType.SpSummon, CardId.AllyOfJustice);
            AddExecutor(ExecutorType.SpSummon, CardId.TGWonderMagician);
            AddExecutor(ExecutorType.SpSummon, CardId.WindUpZenmaines);

            // Side Cards
            AddExecutor(ExecutorType.Activate, CardId.SantaClaws, DefaultSantaClaws);
            AddExecutor(ExecutorType.Activate, CardId.MysticalSpaceTyphoon, MysticalSpaceTyphoonEffect);
            AddExecutor(ExecutorType.Activate, CardId.PoisonousWinds, PoisonousWindsEffect);
            AddExecutor(ExecutorType.Activate, CardId.DrowningMirrorForce, DrowningMirrorForceTrap);
            AddExecutor(ExecutorType.Activate, CardId.DiamondDust, DiamondDustTrap);

            // Common
            AddExecutor(ExecutorType.MonsterSet);
            AddExecutor(ExecutorType.SpSummon);
            AddExecutor(ExecutorType.Repos, DefaultMonsterRepos);
            AddExecutor(ExecutorType.SpellSet);

        }

        private bool PowerBondEffect()
        {
            throw new NotImplementedException();
        }

        private bool FeatherDisterEffect()
        {
            throw new NotImplementedException();
        }

        private bool SurfaceEffect()
        {
            throw new NotImplementedException();
        }

        private bool DarkHoleEffect()
        {
            throw new NotImplementedException();
        }

        private bool MonsterRebornEffect()
        {
            throw new NotImplementedException();
        }

        private bool PotOfDualityEffect()
        {
            throw new NotImplementedException();
        }

        private bool GraydleImpactEffect()
        {
            throw new NotImplementedException();
        }

        private bool StarlightRoadTrap()
        {
            throw new NotImplementedException();
        }

        private bool GraydleSplitTrap()
        {
            throw new NotImplementedException();
        }

        private bool GraydleParasiteTrap()
        {
            throw new NotImplementedException();
        }

        private bool SkillDrainTrap()
        {
            throw new NotImplementedException();
        }

        private bool SolemnJudgmentTrap()
        {
            throw new NotImplementedException();
        }

        private bool DefaultSantaClaws()
        {
            throw new NotImplementedException();
        }

        private bool MysticalSpaceTyphoonEffect()
        {
            throw new NotImplementedException();
        }

        private bool PoisonousWindsEffect()
        {
            throw new NotImplementedException();
        }

        private bool DrowningMirrorForceTrap()
        {
            throw new NotImplementedException();
        }

        private bool DiamondDustTrap()
        {
            throw new NotImplementedException();
        }

        private bool InHand(int id) { return Bot.HasInHand(id); }
        private bool InGraveyard(int id) { return Bot.HasInGraveyard(id); }
        private bool InMonsterZone(int id) { return Bot.HasInMonstersZone(id); }
        private bool IsBanished(int id) { return Bot.HasInBanished(id); }

        public override bool OnSelectHand()
        {
            // go first
            return true;
        }

        private bool DefaultGraydleSlime()
        {
            throw new NotImplementedException();
        }

        private bool DefaultGraydleEagle()
        {
            throw new NotImplementedException();
        }

        private bool DefaultGraydleCobra()
        {
            throw new NotImplementedException();
        }

        private bool DefaultGraydleAlligator()
        {
            throw new NotImplementedException();
        }

        private new bool DefaultMaxxC()
        {
            throw new NotImplementedException();
        }

        private new bool DefaultEffectVeiler()
        {
            throw new NotImplementedException();
        }

    }
}
