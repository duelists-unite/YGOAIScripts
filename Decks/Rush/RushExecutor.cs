using Enumerator;
using System.Collections.Generic;

namespace DuelBot.Game.AI.Decks.Rush
{
    public class RushExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("Rush", DuelRules.RushDuel, (ai, duel) => new RushExecutor(ai, duel),
                "02DYvFuDXXcFAm/djcCHHiFwvosmHB8E8n+Ha7I3ukDwRqBaGFY8q8leDxSryNZk1wKa9wuorsYFgf8A+a8NNNl3A9XuAeKXQLOeAvFbIN63G4L3QvHjzRpgcwA=");
        }
        public class CardId
        {
            // Monsters
            public const int fireGuardian = 120110003;
            public const int darkSorcerer = 120105005;
            public const int dragolite = 120110005;
            public const int lesserDragon = 120120002;
            public const int babyDragon = 120145007;
            public const int lizardSoldier = 120120001;
            public const int yggdrago = 120150011;
            public const int yggdragoL = 120150010;
            public const int yggdragoR = 120150012;
            public const int dragias = 120110001;
            public const int miragias = 120145025;
            public const int zerogias = 120180001;
            public const int burningBlazeDragon = 120145023;
            public const int jinzo = 120155000;
            public const int windcasterTorna = 120105002;
            public const int treasureDragon = 120145020;

            // Spells
            public const int blockAttack = 120140011;
            public const int dragonicPressure = 120110011;
            public const int dragonHeatflash = 120110012;
            public const int hammerCrush = 120120041;
            public const int piercing = 120120037;
            public const int mountain = 120120045;

            // Traps 
            public const int dragonStrike = 120110014;
            public const int dragonEncounter = 120110013;
            public const int dragonTenacity = 120108003;
        }

        List<int> canBeDiscarded = new List<int>
        {
            CardId.lizardSoldier,
            CardId.dragolite,
            CardId.babyDragon,
            CardId.lesserDragon,
            CardId.darkSorcerer,
            CardId.fireGuardian,
            CardId.treasureDragon,
            CardId.windcasterTorna,
            CardId.blockAttack,
            CardId.dragonicPressure,
            CardId.dragonHeatflash,
            CardId.hammerCrush,
            CardId.piercing,
        };

        public RushExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.Activate, CardId.dragonicPressure, pressureEff);
            AddExecutor(ExecutorType.Activate, CardId.mountain);
            AddExecutor(ExecutorType.Activate, CardId.blockAttack, blockAttackEff);
            AddExecutor(ExecutorType.Activate, CardId.hammerCrush, hammerCrushEff);

            AddExecutor(ExecutorType.SpSummon, CardId.yggdrago);
            AddExecutor(ExecutorType.Activate, CardId.yggdrago, yggdragoEff);

            AddExecutor(ExecutorType.Summon, CardId.fireGuardian, fireGuardianSummon);
            AddExecutor(ExecutorType.Summon, CardId.burningBlazeDragon, blazeDragonSummon);
            AddExecutor(ExecutorType.Summon, CardId.windcasterTorna, tornaSummon);
            AddExecutor(ExecutorType.Summon, CardId.darkSorcerer);
            AddExecutor(ExecutorType.Summon, CardId.dragolite);
            AddExecutor(ExecutorType.Summon, CardId.lesserDragon);
            AddExecutor(ExecutorType.Summon, CardId.babyDragon);
            AddExecutor(ExecutorType.Summon, CardId.lizardSoldier);
            AddExecutor(ExecutorType.Summon, CardId.treasureDragon);

            AddExecutor(ExecutorType.Summon, CardId.jinzo, jinzoSummon);

            AddExecutor(ExecutorType.Summon, CardId.zerogias, zerogiasSummon);
            AddExecutor(ExecutorType.Activate, CardId.zerogias, zerogiasEff);
            AddExecutor(ExecutorType.Activate, CardId.dragias, dragiasEff);
            AddExecutor(ExecutorType.Summon, CardId.dragias, dragiasSummon);
            AddExecutor(ExecutorType.Summon, CardId.miragias, miragiasSummon);

            AddExecutor(ExecutorType.SpellSet, CardId.dragonStrike);
            AddExecutor(ExecutorType.Activate, CardId.dragonStrike, dragonStrikeEff);
            AddExecutor(ExecutorType.SpellSet, CardId.dragonEncounter);
            AddExecutor(ExecutorType.Activate, CardId.dragonEncounter, encounterEff);
            AddExecutor(ExecutorType.SpellSet, CardId.dragonTenacity);
            AddExecutor(ExecutorType.Activate, CardId.dragonTenacity, tenacityEff);

            AddExecutor(ExecutorType.Activate, CardId.piercing, piercingEff);
            AddExecutor(ExecutorType.Activate, CardId.dragonHeatflash);
        }

        public override bool OnSelectHand()
        {
            return false;
        }

        public override bool OnPreBattleBetween(BotClientCard attacker, BotClientCard defender)
        {
            if (defender.IsAttack() && attacker.Attack > defender.Attack)
            {
                return true;
            }
            if (defender.IsDefense() && attacker.Attack > defender.Defense)
            {
                return true;
            }
            return false;
        }

        public bool yggdragoEff()
        {
            return true;
        }

        public int getLowestAtk()
        {
            int lowestAtk = 4000;
            List<BotClientCard> monsters = Bot.GetMonsters();
            foreach(BotClientCard monster in monsters)
            {
                if (monster.Attack < lowestAtk)
                {
                    lowestAtk = monster.Attack;
                }
            }
            return lowestAtk;
        }

        public bool jinzoSummon()
        {
            int lowestATK = getLowestAtk();
            foreach(BotClientCard monster in Bot.GetMonsters())
            {
                if (lowestATK < 2400 && !Bot.HasInMonstersZone(CardId.jinzo))
                {
                    AI.SelectCard(monster);
                    return true;
                }
            }
            return false;
        }

        public bool fireGuardianSummon()
        {
            int lowestATK = getLowestAtk();
            foreach(BotClientCard monster in Bot.GetMonsters())
            {
                if (lowestATK < 2100 && !Bot.HasInMonstersZone(CardId.fireGuardian))
                {
                    AI.SelectCard(monster);
                    return true;
                }
            }
            return false;
        }

        public bool blazeDragonSummon()
        {
            int lowestATK = getLowestAtk();
            foreach (BotClientCard monster in Bot.GetMonsters())
            {
                if (lowestATK < 1800 && !Bot.HasInMonstersZone(CardId.burningBlazeDragon))
                {
                    AI.SelectCard(monster);
                    return true;
                }
            }
            return false;
        }

        public bool tornaSummon()
        {
            int lowestATK = getLowestAtk();
            foreach (BotClientCard monster in Bot.GetMonsters())
            {
                if (lowestATK < 1600 && !Bot.HasInMonstersZone(CardId.windcasterTorna))
                {
                    AI.SelectCard(monster);
                    return true;
                }
            }
            return false;
        }

        public bool dragiasSummon()
        {
            int lowestATK = getLowestAtk();
            foreach(BotClientCard monster in Bot.GetMonsters())
            {
                if (lowestATK < 2500 && !Bot.HasInMonstersZone(CardId.dragias))
                {
                    AI.SelectCard(monster);
                }
            }
            foreach(BotClientCard monster in Bot.GetMonsters())
            {
                if (monster.Attack > lowestATK && monster.Attack < 2500)
                {
                    AI.SelectCard(monster);
                    return true;
                }
            }
            return false;
        }

        public bool zerogiasSummon()
        {
            int lowestATK = getLowestAtk();
            foreach (BotClientCard monster in Bot.GetMonsters())
            {
                if (lowestATK < 2500)
                {
                    AI.SelectCard(monster);
                }
            }
            foreach (BotClientCard monster in Bot.GetMonsters())
            {
                if (monster.Attack > lowestATK && monster.Attack < 2500)
                {
                    AI.SelectCard(monster);
                    return true;
                }
            }
            return false;
        }

        public bool miragiasSummon()
        {
            int lowestATK = getLowestAtk();
            foreach (BotClientCard monster in Bot.GetMonsters())
            {
                if (lowestATK < 2500)
                {
                    AI.SelectCard(monster);
                }
            }
            foreach (BotClientCard monster in Bot.GetMonsters())
            {
                if (monster.Attack > lowestATK && monster.Attack < 2500)
                {
                    AI.SelectCard(monster);
                    return true;
                }
            }
            return false;
        }

        public bool zerogiasEff()
        {
            int highestATK = 0;
            foreach (BotClientCard monster in Enemy.GetMonsters())
            {
                if (monster.Attack > highestATK)
                {
                    highestATK = monster.Attack;
                }
            }

            foreach (BotClientCard monster in Enemy.GetMonsters())
            {
                if (monster.Attack == highestATK)
                {
                    AI.SelectCard(monster);
                    return true;
                }
            }
            return false;
        }

        public bool dragiasEff()
        {
            return true;
        }

        public bool tenacityEff()
        {
            int highestLevel = 0;
            foreach(BotClientCard monster in Bot.GetGraveyardMonsters())
            {
                if (monster.Level > highestLevel && monster.HasRace(CardRace.Dragon))
                {
                    highestLevel = monster.Level;
                }
            }

            foreach(BotClientCard monster in Bot.GetGraveyardMonsters())
            {
                if (monster.Level == highestLevel)
                {
                    AI.SelectCard(monster);
                    return true;
                }
            }
            return false;
        }

        public bool piercingEff()
        {
            int highestATK = 0;
            foreach(BotClientCard monster in Bot.GetMonsters())
            {
                if (monster.Attack > highestATK)
                {
                    highestATK = monster.Attack;
                }
            }

            foreach(BotClientCard monster in Bot.GetMonsters())
            {
                if (monster.Attack == highestATK)
                {
                    AI.SelectCard(monster);
                    return true;
                }
            }
            return false;
        }

        public bool blockAttackEff()
        {
            BotClientCard card = Enemy.GetMonsters().GetHighestAttackMonster();
            AI.SelectCard(card);
            return true;
        }

        public bool encounterEff()
        {
            if (Bot.HasInHand(CardId.zerogias) || Bot.HasInHand(CardId.miragias) || Bot.HasInHand(CardId.dragias))
            {
                AI.SelectCard(CardId.zerogias, CardId.miragias, CardId.dragias);
                return true;
            }
            return false;
        }

        public bool dragonStrikeEff()
        {
            if (!Bot.HasInHand(canBeDiscarded))
            {
                return false;
            }
            AI.SelectCard(canBeDiscarded);

            int highestATK = 0;
            foreach (BotClientCard monster in Enemy.GetMonsters())
            {
                if (monster.Attack > highestATK)
                {
                    highestATK = monster.Attack;
                }
            }

            foreach (BotClientCard monster in Enemy.GetMonsters())
            {
                if (monster.Attack == highestATK)
                {
                    AI.SelectNextCard(monster);
                    return true;
                }
            }
            return false;
        }

        public bool hammerCrushEff()
        {
            AI.SelectCard(canBeDiscarded);
            return true;
        }

        public bool pressureEff()
        {
            if (Enemy.GetMonsterCount() == 3 && Bot.GetMonsterCount() == 0)
            {
                return true;
            }
            return false;
        }
    }
}