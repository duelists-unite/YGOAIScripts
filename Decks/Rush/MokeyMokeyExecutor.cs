﻿using Enumerator;
using System.Collections.Generic;
using System.Linq;
using System;

namespace DuelBot.Game.AI.Decks.Rush
{
    public class MokeyMokeyExecutor : DefaultExecutor
    {
        public static void Init()
        {
            DecksManager.AddDeckType("MokeyMokey", DuelRules.RushDuel, (ai, duel) => new RushExecutor(ai, duel),
                "02AQWKbJvnG3BhxrrkDgrbshWHcFBGe6aMKxHpAPw2VAPghfeqQBxw84NeF4J9AMGH4frcluAtQDw3t2Q7DpCgQ2A+JAoLuYIzTZQe4DAA==");
        }
        public class CardId
        {
            public const int 青眼白龙 = 120120000;
            public const int 破坏之剑士 = 120170000;
            public const int 黑魔术师 = 120130000;
            public const int 真红眼黑龙 = 120125001;
            public const int 恶魔召唤 = 120145000;
            public const int 人造人 = 120155000;
            public const int 连击龙 = 120110001;
            public const int 七星道魔术师 = 120105001;
            public const int 雅灭鲁拉 = 120120029;
            public const int 耳语妖精 = 120120018;
            public const int 神秘庄家 = 120105006;
            public const int 火星心少女 = 120145014;
            public const int 斗牛士 = 120170035;
            public const int 凤凰龙 = 120110009;
            public const int 七星道法师 = 120130016;
            public const int sionmax = 120150007;
            public const int 对死者的供奉 = 120151023;
            public const int 落穴 = 120150019;
            public const int 暗黑释放 = 120105013;
        }

        public MokeyMokeyExecutor(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.SpSummon);
            AddExecutor(ExecutorType.Activate, CardId.七星道法师,七星道法师Effect);
            AddExecutor(ExecutorType.Summon, CardId.青眼白龙, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Summon, CardId.破坏之剑士, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Summon, CardId.恶魔召唤, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Summon, CardId.连击龙, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Summon, CardId.雅灭鲁拉, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Summon, CardId.黑魔术师, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Summon, CardId.真红眼黑龙, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Summon, CardId.七星道魔术师, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Summon, CardId.人造人, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Summon, CardId.神秘庄家);
            AddExecutor(ExecutorType.Activate, CardId.神秘庄家);
            AddExecutor(ExecutorType.SpellSet, CardId.暗黑释放);
            AddExecutor(ExecutorType.SpellSet, CardId.落穴);
            AddExecutor(ExecutorType.Activate, CardId.落穴, 落穴Effect);
            AddExecutor(ExecutorType.Activate, CardId.对死者的供奉, 死供Effect);
            AddExecutor(ExecutorType.SpellSet, CardId.对死者的供奉);
            AddExecutor(ExecutorType.MonsterSet, CardId.凤凰龙, monsterset);
            AddExecutor(ExecutorType.MonsterSet, CardId.火星心少女, monsterset);
            AddExecutor(ExecutorType.MonsterSet, CardId.七星道法师, monsterset);
            AddExecutor(ExecutorType.MonsterSet, CardId.耳语妖精, monsterset);
            AddExecutor(ExecutorType.Summon, CardId.七星道法师);
            AddExecutor(ExecutorType.Summon, CardId.凤凰龙);
            AddExecutor(ExecutorType.Activate, CardId.凤凰龙);
            AddExecutor(ExecutorType.SummonOrSet, DefaultMonsterSummon);
            AddExecutor(ExecutorType.Repos, DefaultMonsterRepos);                     
            AddExecutor(ExecutorType.SpellSet);
            //AddExecutor(ExecutorType.Activate, CardId.sionmax, sionmaxEffect);
            AddExecutor(ExecutorType.Activate, CardId.耳语妖精, 耳语妖精Effect);
            AddExecutor(ExecutorType.Activate, CardId.火星心少女, 火星心少女Effect);
            AddExecutor(ExecutorType.Activate, CardId.连击龙);
            AddExecutor(ExecutorType.Activate, CardId.七星道魔术师);
            //AddExecutor(ExecutorType.Activate, CardId.斗牛士, 斗牛士Effect);
            
            AddExecutor(ExecutorType.Activate, DefaultDontChainMyself);

        }



        private List<DHint> DHintForEnemy = new()
        {
            DHint.RELEASE, DHint.DESTROY, DHint.REMOVE, DHint.TOGRAVE, DHint.RTOHAND, DHint.TODECK,
            DHint.FMATERIAL, DHint.SMATERIAL, DHint.XMATERIAL, DHint.LMATERIAL, DHint.DISABLEZONE
        };
        private List<DHint> DHintForMaxSelect = new()
        {
            DHint.SPSUMMON, DHint.TOGRAVE, DHint.ATOHAND, DHint.TODECK, DHint.DESTROY
        };

        public override IList<BotClientCard> OnSelectCard(IList<BotClientCard> _cards, int min, int max, int hint, bool cancelable)
        {
            if (BotDuel.Phase == DuelPhase.BattleStart)
                return null;
            if (AI.HaveSelectedCards())
                return null;

            List<BotClientCard> selected = new();
            List<BotClientCard> cards = new(_cards);
            if (max > cards.Count)
                max = cards.Count;

            if (DHintForEnemy.Contains((DHint)hint))
            {
                var enemyCards = cards.Where(card => card.Controller == 1).ToList();

                // select enemy's card first
                while (enemyCards.Count > 0 && selected.Count < max)
                {
                    var card = enemyCards[UnityEngine.Random.Range(0, enemyCards.Count)];
                    selected.Add(card);
                    enemyCards.Remove(card);
                    cards.Remove(card);
                }
            }

            if (DHintForMaxSelect.Contains((DHint)hint))
            {
                // select max cards
                while (selected.Count < max)
                {
                    var card = cards[UnityEngine.Random.Range(0, cards.Count)];
                    selected.Add(card);
                    cards.Remove(card);
                }
            }

            return selected;
        }
        public override bool OnSelectHand()
        {
            // go first
            return true;
        }
        public bool monsterset()
        {
            if (BotDuel.Turn == 1)
            {
                return true;
            }
            else if (Bot.HasInHand(new[] {
                CardId.七星道魔术师,
                CardId.连击龙,
                CardId.青眼白龙,
                CardId.人造人,
                CardId.恶魔召唤,
                CardId.雅灭鲁拉,
                CardId.破坏之剑士,
                CardId.真红眼黑龙,
                CardId.黑魔术师
                }))
                return false;
            return true;
        }
        private bool 耳语妖精Effect()
        {

            List<BotClientCard> targets = new();
            foreach (var card in Enemy.GetGraveyardMonsters())
            {
                if (card.Level <= 4)
                    targets.Add(card);
            }
            return true;

        }
        private bool 落穴Effect()
        {           
                foreach (var n in BotDuel.LastSummonedCards)
                {
                    if (n.Attack >= 1900)
                        return true;
                }
            
            return false;
        }
        private bool 火星心少女Effect()
        {
            foreach (var m in Bot.Hand)
                AI.SelectCard(m);
            AI.SelectNextCard(Enemy.GetMonsters().GetHighestAttackMonster());
            AI.SelectYesNo(true);
            return true;
        }
        private bool 死供Effect()
        {
            if (Util.IsOneEnemyBetterThanValue(1900, true))
            {
                foreach (var m in Bot.Hand) 
                    AI.SelectCard(m); 
                AI.SelectNextCard(Enemy.GetMonsters().GetHighestAttackMonster());
                return true;
            }
            return false;
        }
        private bool 七星道法师Effect()
        {
            AI.SelectCard(Enemy.GetMonsters().GetHighestAttackMonster());
            return true;
        }




    }
}





