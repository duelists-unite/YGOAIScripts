using Enumerator;
using Extensions;
using Manager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using UnityEngine;
using static ClientAPI.WebServer;

namespace DuelBot.Game.AI.Decks
{
    public enum BotState : byte
    {
        None,
        SelectBattleCmd,
        SelectCard,
        SelectUnselectCard,
        SelectChain,
        SelectCounter,
        SelectDisfield,
        SelectEffectYn,
        SelectIdleCmd,
        SelectOption,
        SelectPlace,
        SelectPosition,
        SelectSum,
        SelectTribute,
        AnnounceAttribute,
        AnnounceCard,
        AnnounceNumber,
        AnnounceRace,
        AnnounceCardFilter,
        WaitingForDeck,
        SelectYesNo,
        RockPaperScissors,
        SortCard
    }
    [Serializable]
    public struct BattleCmdStateData : ISerializable
    {
        public DynamicCard[] AttackableCards;
        public DynamicCard[] ActivableCards;
        public int[] ActivableDescriptions;
        public bool CanMainPhaseTwo;
        public bool CanEndPhase;

        public BattleCmdStateData(BotBattlePhase battle)
        {
            battle.AttackableCards.WriteBotDynamicCardList(out AttackableCards);
            battle.ActivableCards.WriteBotDynamicCardList(out ActivableCards);
            ActivableDescriptions = battle.ActivableDescs.ToArray();
            CanMainPhaseTwo = battle.CanMainPhaseTwo;
            CanEndPhase = battle.CanEndPhase;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("AttackableCards", AttackableCards);
            info.AddValue("ActivableCards", ActivableCards);
            info.AddValue("ActivableDescriptions", ActivableDescriptions);
            info.AddValue("CanMainPhaseTwo", CanMainPhaseTwo);
            info.AddValue("CanEndPhase", CanEndPhase);
        }
    }
    [Serializable]
    public struct SelectYesNoStateData : ISerializable
    {
        public int Description;

        public SelectYesNoStateData(int description)
        {
            Description = description;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Description", Description);
        }
    }
    [Serializable]
    public struct SortCardData : ISerializable
    {
        public DynamicCard[] SelectableCards;

        public SortCardData(IList<BotClientCard> cards)
        {
            cards.WriteBotDynamicCardList(out SelectableCards);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SelectableCards", SelectableCards);
        }
    }
    [Serializable]
    public struct SelectCardStateData : ISerializable
    {
        public DynamicCard[] SelectableCards;
        public int Minimum;
        public int Maximum;
        public int SelectionHint;
        public bool Cancelable;
        public byte SelectableCount;
        public SelectCardStateData(IList<BotClientCard> cards, int min, int max, int hint, bool cancelable, byte count)
        {
            cards.WriteBotDynamicCardList(out SelectableCards);
            Minimum = min;
            Maximum = max;
            SelectionHint = hint;
            Cancelable = cancelable;
            SelectableCount = count;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SelectableCards", SelectableCards);
            info.AddValue("Minimum", Minimum);
            info.AddValue("Maximum", Maximum);
            info.AddValue("SelectionHint", SelectionHint);
            info.AddValue("Cancelable", Cancelable);
            info.AddValue("SelectableCount", SelectableCount);
        }
    }
    [Serializable]
    public struct SelectUnselectCardData : ISerializable
    {
        public DynamicCard[] SelectableCards;
        public int Minimum;
        public int Maximum;
        public int SelectionHint;
        public bool Cancelable;
        public byte SelectableCount;
        public SelectUnselectCardData(IList<BotClientCard> cards, int min, int max, int hint, bool cancelable, byte count)
        {
            cards.WriteBotDynamicCardList(out SelectableCards);
            Minimum = min;
            Maximum = max;
            SelectionHint = hint;
            Cancelable = cancelable;
            SelectableCount = count;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SelectableCards", SelectableCards);
            info.AddValue("Minimum", Minimum);
            info.AddValue("Maximum", Maximum);
            info.AddValue("SelectionHint", SelectionHint);
            info.AddValue("Cancelable", Cancelable);
            info.AddValue("SelectableCount", SelectableCount);
        }
    }
    [Serializable]
    public struct SelectChainData : ISerializable
    {
        public DynamicCard[] SelectableCards;
        public int[] Descriptions;
        public bool Forced;

        public SelectChainData(IList<BotClientCard> cards, IList<int> descs, bool forced)
        {
            cards.WriteBotDynamicCardList(out SelectableCards);
            Descriptions = descs.ToArray();
            Forced = forced;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SelectableCards", SelectableCards);
            info.AddValue("Descriptions", Descriptions);
            info.AddValue("Forced", Forced);
        }
    }
    [Serializable]
    public struct SelectCounterData : ISerializable
    {
        public short RequiredType;
        public short RequiredQuantity;
        public DynamicCard[] SelectableCards;
        public short[] Counters;

        public SelectCounterData(short type, short quantity, IList<BotClientCard> cards, IList<short> counters)
        {
            RequiredType = type;
            RequiredQuantity = quantity;
            cards.WriteBotDynamicCardList(out SelectableCards);
            Counters = counters.ToArray();
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("RequiredType", RequiredType);
            info.AddValue("RequiredQuantity", RequiredQuantity);
            info.AddValue("SelectableCards", SelectableCards);
            info.AddValue("Counters", Counters);
        }
    }
    [Serializable]
    public struct SelectDisfieldData : ISerializable
    {
        public int CardID;
        public byte Player;
        public short Location;
        public int Filter;

        public SelectDisfieldData(int cardId, int player, CardLocation location, int available)
        {
            CardID = cardId;
            Player = (byte)player;
            Location = (short)location;
            Filter = available;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("CardID", CardID);
            info.AddValue("Player", Player);
            info.AddValue("Location", Location);
            info.AddValue("Filter", Filter);
        }
    }
    [Serializable]
    public struct SelectEffectYnData : ISerializable
    {
        public DynamicCard EffectCard;
        public int Description;

        public SelectEffectYnData(BotClientCard card, int desc)
        {
            EffectCard = new(card);
            Description = desc;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("EffectCard", EffectCard);
            info.AddValue("Description", Description);
        }
    }
    [Serializable]
    public struct SelectOptionData : ISerializable
    {
        public int[] Options;

        public SelectOptionData(IList<int> options)
        {
            Options = options.ToArray();
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Options", Options);
        }
    }
    [Serializable]
    public struct SelectPlaceData : ISerializable
    {
        public int CardID;
        public byte Player;
        public short Location;
        public int Filter;

        public SelectPlaceData(int cardId, int player, CardLocation location, int available)
        {
            CardID = cardId;
            Player = (byte)player;
            Location = (short)location;
            Filter = available;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("CardID", CardID);
            info.AddValue("Player", Player);
            info.AddValue("Location", Location);
            info.AddValue("Filter", Filter);
        }
    }
    [Serializable]
    public struct SelectPositionData : ISerializable
    {
        public int CardID;
        public byte[] Positions;

        public SelectPositionData(int cardId, IList<CardPosition> positions)
        {
            CardID = cardId;
            Positions = positions.Select(cp => (byte)cp).ToArray();
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("CardID", CardID);
            info.AddValue("Positions", Positions);
        }
    }
    [Serializable]
    public struct SelectSumData : ISerializable
    {
        public DynamicCard[] SelectableCards;
        public int Sum;
        public int Minimum;
        public int Maximum;
        public int SelectionHint;
        public bool MustBeExactValue;
        public DynamicCard[] MandatoryCards;

        public SelectSumData(IList<BotClientCard> cards, int sum, int min, int max, int hint, bool mode, IList<BotClientCard> mandatoryCards)
        {
            cards.WriteBotDynamicCardList(out SelectableCards);
            Sum = sum;
            Minimum = min;
            Maximum = max;
            SelectionHint = hint;
            MustBeExactValue = mode;
            mandatoryCards.WriteBotDynamicCardList(out MandatoryCards);
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SelectableCards", SelectableCards);
            info.AddValue("Sum", Sum);
            info.AddValue("Minimum", Minimum);
            info.AddValue("Maximum", Maximum);
            info.AddValue("SelectionHint", SelectionHint);
            info.AddValue("MustBeExactValue", MustBeExactValue);
            info.AddValue("MandatoryCards", MandatoryCards);
        }
    }
    [Serializable]
    public struct SelectTributeData : ISerializable
    {
        public DynamicCard[] SelectableCards;
        public int Minimum;
        public int Maximum;
        public int SelectionHint;
        public bool Cancelable;
        public byte SelectableCount;
        public SelectTributeData(IList<BotClientCard> cards, int min, int max, int hint, bool cancelable, byte count)
        {
            cards.WriteBotDynamicCardList(out SelectableCards);
            Minimum = min;
            Maximum = max;
            SelectionHint = hint;
            Cancelable = cancelable;
            SelectableCount = count;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SelectableCards", SelectableCards);
            info.AddValue("Minimum", Minimum);
            info.AddValue("Maximum", Maximum);
            info.AddValue("SelectionHint", SelectionHint);
            info.AddValue("Cancelable", Cancelable);
            info.AddValue("SelectableCount", SelectableCount);
        }
    }
    [Serializable]
    public struct AnnounceAttributeData : ISerializable
    {
        public int RequiredCount;
        public byte[] SelectableOptions;

        public AnnounceAttributeData(int count, IList<CardAttribute> attributes)
        {
            RequiredCount = count;
            SelectableOptions = attributes.Select(a => (byte)a).ToArray();
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("RequiredCount", RequiredCount);
            info.AddValue("SelectableOptions", SelectableOptions);
        }
    }
    [Serializable]
    public struct AnnounceCardData : ISerializable
    {
        public byte FilterCount;
        public int[] AppliableFilters;

        public AnnounceCardData(byte count, int[] filters)
        {
            FilterCount = count;
            AppliableFilters = filters;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("FilterCount", FilterCount);
            info.AddValue("AppliableFilters", AppliableFilters);
        }
    }
    [Serializable]
    public struct AnnounceNumberData : ISerializable
    {
        public int[] SelectableOptions;

        public AnnounceNumberData(IList<int> numbers)
        {
            SelectableOptions = numbers.ToArray();
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SelectableOptions", SelectableOptions);
        }
    }
    [Serializable]
    public struct AnnounceRaceData : ISerializable
    {
        public int RequiredCount;
        public int[] SelectableOptions;

        public AnnounceRaceData(int count, IList<CardRace> races)
        {
            RequiredCount = count;
            SelectableOptions = races.Select(r => (int)r).ToArray();
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("RequiredCount", RequiredCount);
            info.AddValue("SelectableOptions", SelectableOptions);
        }
    }
    [Serializable]
    public struct AnnounceCardFilterData : ISerializable
    {
        public byte FilterCount;
        public int[] AppliableFilters;

        public AnnounceCardFilterData(byte count, int[] filters)
        {
            FilterCount = count;
            AppliableFilters = filters;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("FilterCount", FilterCount);
            info.AddValue("AppliableFilters", AppliableFilters);
        }
    }
    [Serializable]
    public struct IdleCmdStateData : ISerializable
    {
        public DynamicCard[] SummonableCards;
        public DynamicCard[] SpecialSummonableCards;
        public DynamicCard[] ReposableCards;
        public DynamicCard[] MonsterSetableCards;
        public DynamicCard[] SpellSetableCards;
        public DynamicCard[] ActivableCards;
        public int[] ActivableDescriptions;
        public bool CanBattlePhase;
        public bool CanEndPhase;
        public bool CanShuffleHand;

        public IdleCmdStateData(BotMainPhase main)
        {
            main.SummonableCards.WriteBotDynamicCardList(out SummonableCards);
            main.SpecialSummonableCards.WriteBotDynamicCardList(out SpecialSummonableCards);
            main.ReposableCards.WriteBotDynamicCardList(out ReposableCards);
            main.MonsterSetableCards.WriteBotDynamicCardList(out MonsterSetableCards);
            main.SpellSetableCards.WriteBotDynamicCardList(out SpellSetableCards);
            main.ActivableCards.WriteBotDynamicCardList(out ActivableCards);
            ActivableDescriptions = main.ActivableDescs.ToArray();
            CanBattlePhase = main.CanBattlePhase;
            CanEndPhase = main.CanEndPhase;
            CanShuffleHand = main.CanShuffle;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SummonableCards", SummonableCards);
            info.AddValue("SpecialSummonableCards", SpecialSummonableCards);
            info.AddValue("ReposableCards", ReposableCards);
            info.AddValue("MonsterSetableCards", MonsterSetableCards);
            info.AddValue("SpellSetableCards", SpellSetableCards);
            info.AddValue("ActivableCards", ActivableCards);
            info.AddValue("ActivableDescriptions", ActivableDescriptions);
            info.AddValue("CanBattlePhase", CanBattlePhase);
            info.AddValue("CanEndPhase", CanEndPhase);
            info.AddValue("CanShuffleHand", CanShuffleHand);
        }
    }
    [Serializable]
    public struct BotGeneralStateData : ISerializable
    {
        public bool IsReady;
        public BotGeneralStateData(bool ready)
        {
            IsReady = ready;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("IsReady", IsReady);
        }
    }
    public class AI_Custom : Executor
    {      
        public static void Init()
        {
            DecksManager.AddDeckType("AI_Custom", DuelRules.MasterDuel, (ai, duel) => new AI_Custom(ai, duel), string.Empty, false, true);
            DecksManager.AddDeckType("AI_Custom", DuelRules.SpeedDuel, (ai, duel) => new AI_Custom(ai, duel), string.Empty, false, true);
            DecksManager.AddDeckType("AI_Custom", DuelRules.RushDuel, (ai, duel) => new AI_Custom(ai, duel), string.Empty, false, true);
        }
        public BattleCmdStateData CurrentBattleCmdStateData { get; private set; }
        public BotGeneralStateData CurrentBotGeneralStateData { get; private set; }
        public IdleCmdStateData CurrentIdleCmdStateData { get; private set; }
        public SelectCardStateData CurrentSelectCardData { get; private set; }
        public SelectUnselectCardData CurrentSelectUnselectCardData { get; private set; }
        public SelectChainData CurrentSelectChainData { get; private set; }
        public SelectCounterData CurrentSelectCounterData { get; private set; }
        public SelectDisfieldData CurrentSelectDisfieldData { get; private set; }
        public SelectEffectYnData CurrentSelectEffectYnData { get; private set; }
        public SelectOptionData CurrentSelectOptionData { get; private set; }
        public SelectPlaceData CurrentSelectPlaceData { get; private set; }
        public SelectPositionData CurrentSelectPositionData { get; private set; }
        public SelectSumData CurrentSelectSumData { get; private set; }
        public SelectTributeData CurrentSelectTributeData { get; private set; }
        public AnnounceAttributeData CurrentAnnounceAttributeData { get; private set; }
        public AnnounceCardData CurrentAnnounceCardData { get; private set; }
        public AnnounceNumberData CurrentAnnounceNumberData { get; private set; }
        public AnnounceRaceData CurrentAnnounceRaceData { get; private set; }
        public AnnounceCardFilterData CurrentAnnounceCardFilterData { get; private set; }
        public SelectYesNoStateData CurrentSelectYesNoStateData {  get; private set; }
        public SortCardData CurrentSortCardData { get; private set; }

        public BotState CurrentState { get; private set; }
        public string CurrentStateName { get
            {
                return CurrentState switch
                {
                    BotState.SelectBattleCmd => "Select Battle Command",
                    BotState.SelectCard => "Select Card",
                    BotState.SelectUnselectCard => "Select Unselect Card",
                    BotState.SelectChain => "Select Chain",
                    BotState.SelectCounter => "Select Counter",
                    BotState.SelectDisfield => "Select Disfield",
                    BotState.SelectEffectYn => "Select Effect Yes or No",
                    BotState.SelectIdleCmd => "Select Idle Command",
                    BotState.SelectOption => "Select Option",
                    BotState.SelectPlace => "Select Place",
                    BotState.SelectPosition => "Select Position",
                    BotState.SelectSum => "Select Sum",
                    BotState.SelectTribute => "Select Tribute",
                    BotState.AnnounceAttribute => "Announce Attribute",
                    BotState.AnnounceCard => "Announce Card",
                    BotState.AnnounceNumber => "Announce Number",
                    BotState.AnnounceRace => "Announce Race",
                    BotState.AnnounceCardFilter => "Announce Card Filter",
                    BotState.WaitingForDeck => "Waiting for Deck",
                    BotState.SelectYesNo => "Select Yes or No",
                    BotState.RockPaperScissors => "Rock Paper Scissors",
                    BotState.SortCard => "Sort Card",
                    BotState.None or _ => "None",
                };
            } }
        public AI_Custom(GameAI ai, BotDuel duel)
            : base(ai, duel)
        {
            CurrentBotGeneralStateData = new(false);
            CurrentState = BotState.None;
        }

        public void WaitForDeck()
        {
            CurrentBotGeneralStateData = new(false);
            CurrentState = BotState.WaitingForDeck;            
        }
        
        public void OnDeckSet()
        {
            CurrentBotGeneralStateData = new(true);
            CurrentState = BotState.None;           
        }

        public bool CustomResponse(byte[] data)
        {
            if (data == null || data.Length <= 0)
                return false;
            bool result = false;
            try
            {
                using MemoryStream ms = new(data);
                using BinaryReader br = new(ms);
                result = CurrentState switch
                {
                    BotState.SelectBattleCmd => CustomSelectBattleCmdResponse(br),
                    BotState.SelectCard => CustomSelectCardResponse(br),
                    BotState.SelectUnselectCard => CustomSelectUnselectCardResponse(br),                   
                    BotState.SelectChain => CustomSelectChainResponse(br),
                    BotState.SelectCounter => CustomSelectCounterResponse(br),
                    BotState.SelectDisfield => CustomSelectDisfieldResponse(br),
                    BotState.SelectEffectYn => CustomSelectEffectYnResponse(br),
                    BotState.SelectIdleCmd => CustomSelectIdleCmdResponse(br),
                    BotState.SelectOption => CustomSelectOptionResponse(br),
                    BotState.SelectPlace => CustomSelectPlaceResponse(br),
                    BotState.SelectPosition => CustomSelectPositionResponse(br),
                    BotState.SelectSum => CustomSelectSumResponse(br),
                    BotState.SelectTribute => CustomSelectTributeResponse(br),
                    BotState.AnnounceAttribute => CustomAnnounceAttributeResponse(br),
                    BotState.AnnounceCard => CustomAnnounceCardResponse(br),
                    BotState.AnnounceNumber => CustomAnnounceNumberResponse(br),
                    BotState.AnnounceRace => CustomAnnounceRaceResponse(br),
                    BotState.AnnounceCardFilter => CustomAnnounceCardFilterResponse(br),
                    BotState.SelectYesNo => CustomSelectYesNoResponse(br),
                    BotState.RockPaperScissors => CustomRockPaperScissorsResponse(br),
                    BotState.SortCard => CustomSortCardResponse(br),
                    BotState.None or BotState.WaitingForDeck or _ => false,
                };
                CurrentState = BotState.None;
            } catch (Exception ex)
            {
                Debug.Log(ex.Message);
                result = false;
            }
            return result;
        }
        public void CustomSortCardRequest(IList<BotClientCard> cards)
        {
            CurrentState = BotState.SortCard;
            CurrentSortCardData = new(cards);
        }

        private bool CustomSortCardResponse(BinaryReader packet)
        {
            byte count = packet.ReadByte(); // Selection size
            byte[] response = new byte[count];
            for (byte b = 0; b < count; ++b)
                response[b] = packet.ReadByte();
            return SendCustomResponse(response);
        }

        public void CustomRockPaperScissorsRequest()
        {
            CurrentState = BotState.RockPaperScissors;
        }

        private bool CustomRockPaperScissorsResponse(BinaryReader packet)
        {
            byte selection = packet.ReadByte(); // 1 = scissors, 2 = rock, 3 = paper
            return SendCustomResponse(selection);
        }

        public void CustomSelectYesNoRequest(int description)
        {
            CurrentState = BotState.SelectYesNo;
            CurrentSelectYesNoStateData = new(description);
        }

        private bool CustomSelectYesNoResponse(BinaryReader packet)
        {
            bool response = packet.ReadByte() != 0; // 1 for true (yes) and 0 for false (no)
            return SendCustomResponse(response ? 1 : 0);
        }

        public void CustomSelectBattleCmdRequest(BotBattlePhase battle)
        {
            CurrentBattleCmdStateData = new(battle);
            CurrentState = BotState.SelectBattleCmd;            
        }
        private bool EnsureConnection()
        {
            return AI != null && AI.Game != null && AI.Game.Connection != null;
        }
        private bool SendCustomResponse(int response)
        {
            if (!EnsureConnection())
                return false;
            AI.Game.Connection.Send(CtosMessage.Response, response);
            return true;
        }
        private bool SendCustomResponse(byte[] response)
        {
            if (!EnsureConnection())
                return false;
            AI.Game.Connection.Send(CtosMessage.Response, response);
            return true;
        }
        private bool CustomSelectBattleCmdResponse(BinaryReader packet)
        {
            BattleAction ba = (BattleAction)packet.ReadUInt16(); // action of battle phase
            ushort idx = packet.ReadUInt16(); // index of action if applicable (usually got from the card indexes) or 0 if not needed
            BattlePhaseAction bpa = new(ba, idx);
            return SendCustomResponse(bpa.ToValue());
        }

        public void CustomSelectCardRequest(IList<BotClientCard> cards, int min, int max, int hint, bool cancelable, byte count)
        {
            CurrentSelectCardData = new(cards, min, max, hint, cancelable, count);
            CurrentState = BotState.SelectCard;
        }

        private bool CustomSelectCardResponse(BinaryReader packet)
        {
            return CustomInternalSelectCardResponse(packet);
        }

        private bool CustomInternalSelectCardResponse(BinaryReader packet)
        {
            byte count = packet.ReadByte(); // amount of cards selected
            if (count <= 0) // if count is 0 then cancel selection
            {
                return SendCustomResponse(-1); // don't select any card
            }
            byte[] response = new byte[count + 1];
            response[0] = count; 
            for (byte idx = 1; idx <= count; ++idx)
            {
                response[idx] = packet.ReadByte(); // index of the card on the possible selection list that was picked
            }           
            return SendCustomResponse(response);
        }
        public void CustomSelectUnselectCardRequest(IList<BotClientCard> cards, int min, int max, int hint, bool cancelable, byte count)
        {
            CurrentSelectUnselectCardData = new(cards, min, max, hint, cancelable, count);
            CurrentState = BotState.SelectUnselectCard;
        }

        private bool CustomSelectUnselectCardResponse(BinaryReader packet)
        {
            return CustomInternalSelectCardResponse(packet);
        }

        public void CustomSelectChainRequest(IList<BotClientCard> cards, IList<int> descs, bool forced)
        {
            CurrentSelectChainData = new(cards, descs, forced);
            CurrentState = BotState.SelectChain;
        }

        private bool CustomSelectChainResponse(BinaryReader packet)
        {
            int selection = packet.ReadInt32(); // index of the selected chain to activate or -1 for none
            return SendCustomResponse(selection);
        }

        public void CustomSelectCounterRequest(short type, short quantity, IList<BotClientCard> cards, IList<short> counters)
        {
            CurrentSelectCounterData = new(type, quantity, cards, counters);
            CurrentState = BotState.SelectCounter;
        }

        private bool CustomSelectCounterResponse(BinaryReader packet)
        {
            byte count = packet.ReadByte(); // amount of counters selected
            short[] selected = new short[count];
            for (int i = 0; i < count; i++)
                selected[i] = packet.ReadInt16(); // amount of counters selected from this given counter index
            AI.Game.PostSelectCounter(selected);
            return true;
        }

        public void CustomSelectDisfieldRequest(int cardId, int player, CardLocation location, int available)
        {
            CurrentSelectDisfieldData = new(cardId, player, location, available);
            CurrentState = BotState.SelectDisfield;
        }

        private bool CustomSelectDisfieldResponse(BinaryReader packet)
        {
            int selected = packet.ReadInt32(); // selected zone
            AI.Game.PostSelectPlace(selected, CurrentSelectPlaceData.Player, (CardLocation)CurrentSelectPlaceData.Location, CurrentSelectPlaceData.Filter);
            return true;
        }

        public void CustomSelectEffectYnRequest(BotClientCard card, int desc)
        {
            CurrentSelectEffectYnData = new(card, desc);
            CurrentState = BotState.SelectEffectYn;
        }

        private bool CustomSelectEffectYnResponse(BinaryReader packet)
        {
            bool response = packet.ReadByte() != 0; // 1 for yes (true) and 0 for no (false)
            return SendCustomResponse(response ? 1 : 0);
        }

        public void CustomSelectIdleCmdRequest(BotMainPhase main)
        {
            CurrentIdleCmdStateData = new(main);
            CurrentState = BotState.SelectIdleCmd;
        }

        private bool CustomSelectIdleCmdResponse(BinaryReader packet)
        {
            MainAction ma = (MainAction)packet.ReadUInt16(); // action on main phase
            ushort index = packet.ReadUInt16(); // index for the action (usually picked from card indexes or simply 0 if not applicable)
            MainPhaseAction mpa = new(ma, index);
            return SendCustomResponse(mpa.ToValue());
        }

        public void CustomSelectOptionRequest(IList<int> options)
        {
            CurrentSelectOptionData = new(options);
            CurrentState = BotState.SelectOption;
        }

        private bool CustomSelectOptionResponse(BinaryReader packet)
        {
            int response = packet.ReadInt32(); // index of the selected option
            return SendCustomResponse(response);
        }

        public void CustomSelectPlaceRequest(int cardId, int player, CardLocation location, int available)
        {
            CurrentSelectPlaceData = new(cardId, player, location, available);
            CurrentState = BotState.SelectPlace;
        }

        private bool CustomSelectPlaceResponse(BinaryReader packet)
        {
            int selected = packet.ReadInt32(); // selected zone
            AI.Game.PostSelectPlace(selected, CurrentSelectPlaceData.Player, (CardLocation)CurrentSelectPlaceData.Location, CurrentSelectPlaceData.Filter);
            return true;
        }

        public void CustomSelectPositionRequest(int cardId, IList<CardPosition> positions)
        {
            CurrentSelectPositionData = new(cardId, positions);
            CurrentState = BotState.SelectPosition;
        }

        private bool CustomSelectPositionResponse(BinaryReader packet)
        {
            byte position = packet.ReadByte(); // position selected
            return SendCustomResponse(position);
        }

        public void CustomSelectSumRequest(IList<BotClientCard> cards, int sum, int min, int max, int hint, bool mode, IList<BotClientCard> mandatoryCards)
        {
            CurrentSelectSumData = new(cards, sum, min, max, hint, mode, mandatoryCards);
            CurrentState = BotState.SelectSum;
        }

        private bool CustomSelectSumResponse(BinaryReader packet)
        {
            byte count = packet.ReadByte(); // amount of selected cards
            byte[] response = new byte[count + CurrentSelectSumData.MandatoryCards.Length + 1];
            byte index = 0;
            response[index++] = (byte)(response.Length - 1);
            while (index <= CurrentSelectSumData.MandatoryCards.Length)
            {
                response[index++] = 0;
            }
            byte left = 0;
            while (left < count)
            {
                byte selected = packet.ReadByte(); // index of the card selected into the selectable cards list
                response[index++] = CurrentSelectSumData.SelectableCards[selected].SelectSeq;
                ++left;
            }
            return SendCustomResponse(response);
        }

        public void CustomSelectTributeRequest(IList<BotClientCard> cards, int min, int max, int hint, bool cancelable, byte count)
        {
            CurrentSelectTributeData = new(cards, min, max, hint, cancelable, count);
            CurrentState = BotState.SelectTribute;
        }

        private bool CustomSelectTributeResponse(BinaryReader packet)
        {
            return CustomInternalSelectCardResponse(packet);
        }

        public void CustomAnnounceAttributeRequest(int count, IList<CardAttribute> attributes)
        {
            CurrentAnnounceAttributeData = new(count, attributes);
            CurrentState = BotState.AnnounceAttribute;
        }

        private bool CustomAnnounceAttributeResponse(BinaryReader packet)
        {
            int response = packet.ReadInt32(); // 4 bytes mask of selected attributes
            return SendCustomResponse(response);
        }

        public void CustomAnnounceCardRequest(byte count, int[] filters)
        {
            CurrentAnnounceCardData = new(count, filters);
            CurrentState = BotState.AnnounceCard;
        }

        private bool CustomAnnounceCardResponse(BinaryReader packet)
        {
            return CustomInternalAnnounceCardResponse(packet);
        }

        private bool CustomInternalAnnounceCardResponse(BinaryReader packet)
        {
            int cardId = packet.ReadInt32(); // id of the selected card to announce
            return SendCustomResponse(cardId);
        }

        public void CustomAnnounceNumberRequest(IList<int> numbers)
        {
            CurrentAnnounceNumberData = new(numbers);
            CurrentState = BotState.AnnounceNumber;
        }

        private bool CustomAnnounceNumberResponse(BinaryReader packet)
        {
            int index = packet.ReadInt32(); // index of the selected number into the selectable number list
            return SendCustomResponse(index);
        }

        public void CustomAnnounceRaceRequest(int count, IList<CardRace> races)
        {
            CurrentAnnounceRaceData = new(count, races);
            CurrentState = BotState.AnnounceRace;
        }

        private bool CustomAnnounceRaceResponse(BinaryReader packet)
        {
            int response = packet.ReadInt32(); // 4 bytes mask of selected races
            return SendCustomResponse(response);
        }

        public void CustomAnnounceCardFilterRequest(byte count, int[] filters)
        {
            CurrentAnnounceCardFilterData = new(count, filters);
            CurrentState = BotState.AnnounceCardFilter;
        }

        private bool CustomAnnounceCardFilterResponse(BinaryReader packet)
        {
            return CustomInternalAnnounceCardResponse(packet);
        }
    }
}
